# Supplementary materials for *Endosperm turgor pressure both promotes and restricts seed growth and size*

## About
This repository gathers the code we use to analyze the experimental data and the mathematical model presented in the manuscript *Endosperm turgor pressure both promotes and restricts seed growth and size*.

You will find, below, guidelines to install the code, download the raw data and rerun our simulations and analysis.

### Authors
- Of the paper and the work:
  - Audrey Creff
  - Olivier Ali ([corresponding](olivier.ali@inria.fr))
  - Vincent Bayle
  - Gwyneth Ingram ([corresponding](gwyneth.ingram@ens-lyon.fr))
  - Benoit Landrein ([corresponding](benoit.landrein@ens-lyon.fr))
- Of this repository specifically:
  - Olivier Ali ([contact](olivier.ali@inria.fr))

### Main dependencies
- numpy
- scipy
- pandas
- seaborn
- pypet

## 1. Install

> **Note:**
We developed our numerical pipeline within **macOs** and run our parameter space exploration on a **linux** server. The procedure described below should therefore work on such operating systems.

### Code
You will need `conda` in order to install and run our simulations, you can download it  [here](https://docs.conda.io/en/latest/miniconda.html).

if you are not familiar with `conda`, you can find documentation [here](https://docs.conda.io/projects/conda/en/latest/user-guide/concepts/index.html).

You also need `git` but if you run a linux or macOs distribution, you should have it already on your computer. If not, you can find informations and install procedures [here](https://git-scm.com).

Once `conda` is installed on your computer, run the following lines to clone this git repository and create a dedicated conda environment:


    $: git clone https://gitlab.inria.fr/mosaic/publications/seed_sup_mat.git
    $: cd seed_sup_mat
    $: conda env create -f environment.yaml -n seed
    $: conda activate seed

### Data
Once the code has been fetched from the repository and the conda environment is set, you need to download the data sets on which the various pieces of code run. This data set is freely available on **Zenodo**, you can find the repository [here](https://zenodo.org/record/4620948#.YFR1CC1h0UF).

To download it directly in the right place:

    $: cd PATH_TO_REPO
    $: cd model
    $: wget https://zenodo.org/record/4620948/files/data.zip?download=1
    $: mv data.zip?download=1 data.zip
    $: unzip data.zip
    $: rm  -rf __MACOSX

> **Note:** The `PATH_TO_REPO` variable should be replaced by the actual path to the folder where the package has be cloned. This path should end with `/seed_sup_mat/`.

At this point, you are all set to start exploring our notebooks and simulations.

## 2. Explore our work

> **Note:** Within the `seed_sup_mat` repository you will find two modules:
> - `image_analysis/`: This submodule contains **ImageJ** routines developed to analyse experimentally obtained images and extract quantitative measurements. These routines are written in `javascrpit`.
> - `model/`: This submodule gather all the exploration and analysis of our mathematical model. This analysis has been implemtend in **python 3.7** and encompasses both python script files and `jupyter notebooks`.
>
> In the following section you will only find instructions concerning the `model/` module.
>
> All files (data, scripts, notebooks) provided in this repository constitute our analysis workflow and should be sufficient to reproduce all the simulation-related figures of our manuscript (main text and supplementary material). The relationships between all these files is depicted in the figure below.

![data_workflow_figure](./data_workflow_figure.png)


You will find two kinds of files within the `model/` module:

- **python scripts**, within the `/model/script` submodule — extension `.py`.
- **jupyter notebooks**, within the `/model/notebook` submodule — extension `.ipynb`.

The formers can be used to perform a parameter space exploration on our system of *ODEs*.
The latters contain pipelines to analyze the results of the parameter space exploration and to compare them to experimental measurements.



### 2.1. Parameter space exploration

> **Note:** The parameter space exploration we perfomed and present in our manuscript runs over half a million sampled sets of parameter values. It is very demanding in terms of computational resources and cannot be executed on a *regular* computer in a decent amount of time. We therefore parametrized our script (`/model/script/parameter_space_exploration.py`) so it can run on a server with 20 parallelized CPUs. On such an hardware we still had to split the explored parameter space volume into four subregions, one for each value (3, 5, 7, 9) of the `eta` parameter. Each of the corresponding run took around 20 minutes.

#### Run the exploration
We of course encourage the interested reader to rerun this parameter space exploration (this is the point of this open repository) but please, be advised that it can take time. To do so, once the installation procedure is completed, run the following lines in a new terminal window:

    $: conda activate seed
    $: cd PATH_TO_REPO
    $: cd model/script
    $: python explore_parameter_space.py

This is enough to run the parameter space exploration with constant pressure function in the model.

Note that you can modulate directly inline some parameters of the simulation by adding some arguments directly in the command line, for instance:

```bash
$: python explore_parameter_space.py -n CORE_NBR -e ETA -p PFUNC
```

where

- `CORE_NBR` (integer) should be replaced by the number of CPU core you want to use. Default value is 20.
- `ETA` (integer) should be replaced by the value of the eta parameter you want to consider for this specific run. Default is 7.
- `PFUNC` (string) should be replaced by the type of pressure function that you want. It should be either:
  - `cste` to use constant pressure value. 
  -  `generic_drop` to use the `generic_presssure_drop` function (defined in the `utils.py` module).
  - `pwl_drop` to use the `pwl_pressure_drop` function (defined in the `utils.py` module).


> **Notes:** 
>
> * you can omit one or all of these arguments. In this case, default values `CORE_NBR=20` ,  `ETA=7`, `PFUNC=cste` will be used.
>
> * you can also add the argument `t` to run a *test* exploration defined on a smaller region of the parameter space and therefore performed faster. To check that the script is running as intended.

However, if you just want to assess our pipeline on a (very) small volume of the parameter space, you can do it by using the `-t` (for *test*) argument:

    $: python parameter_space_exploration.py -t

This should be runnable on a *regular* computer in a matter of seconds or minutes.

#### Format the results
Once the parameter space exploration has been ran, outputs are recorded within the `data/data_param_space_explo/` folder. Each run produces a folder named `YYMMDD_hhmmss_seed_growth` with `YY`, `MM` and containing the raw data `/hdf5/raw_trajectory.hdf5`.

In order to analyze these results within our notebooks, you need first to format them with the `result_formatting.py` script. To that end run the following lines within a new terminal window:

    $: conda activate seed
    $: cd PATH_TO_REPO
    $: cd model/script
    $: python format_raw_result.py -i YYMMDD_hhmmss_seed_growth

Where `YYMMDD_hhmmss_seed_growth` is the sub-folder containing the `raw_trajectory.hdf5` to process and located within the `/data/data_param_space_explo/` directory.

This script will return a `formatted_dataframe.hdf5` file, located within the same direction as the original `raw_trajectory.hdf5` one.

> **Note:** This step is by far the most time-consumming one. For the full parameter space exploration the formatting of each of the `raw_trajectory.hdf5` files (corresponding each to one quarter of the total explored volume) takes around 2 hours on a *regular* computer.

Once this formatting step is performed, the parameter space exploration phase is completed and you can start analyzing the results within the notebooks.

> **Note:** If you do not want to run this parameter space exploration but are interested in the output processing and analysis, you can skip this part and you the data we generated ourselves, archived within the **Zenodo** repository ([link](https://zenodo.org/record/4620948#.YFR1CC1h0UF)).

### 2.2. Result analysis

Analysis of the outputs of the parameter space exploration and comparison with experimental measurements are performed within **jupyter notebooks**, located within the `model/notebook/` submodule.

These notebooks constitute a pipeline where the outputs of one are used as inputs by others. They therefore need to be executed in the following specific order:
1. `experiment_seed_growth_tracking_analysis.ipynb`: The goal here is to format experimental data from seed size tracking experiments so they can be compared to numerical simulation results.

2. `parameter_space_exploration.ipynb`: Here we extract from all the simulations performed during the parameter space exploration the ones that are compatible with experimental data.

3. `influence_various_pressure_values.ipynb`: We investigate how simulations, parametrized with the parameter sets selected at the previous set react to various values of pressure.

4. `influence_pressure_drop.ipynb`: We look at the system behavior when pressure is not constant anymore but decrease with timee.

> **Note:**
> - Steps 3. and 4. can be interverted.
> - Once outputs have been generated (or download from the **Zenodo** archive) the ordering is not mendatory anymore.

To start using these notebooks, run the following lines within a new terminal window:

    $: conda activate seed
    $: cd PATH_TO_REPO
    $: cd model/notebook
    $: jupyter notebook

At this point a new window (or tab) of your default web browser should open with the list of the four notebooks (plus a `utils.py` file) displayed. To open any notebook, simply click on it.

If you are not familiar with **jupyter notebooks**, you can find documentation [here](https://jupyter.org/documentation).

All notebooks are rather documented and should be easy to follow.

> **Note:**
>
> ​	A fifth file, `steady_state_analysis.ipynb`, is available within the notebook folder: It gathers some work 	performed on the stationnary version of our system of *ODEs*. We notably assessed the influence of various parameters on the existence of the steady state region.
