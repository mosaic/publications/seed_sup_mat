{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Experimental data analysis\n",
    "### Abstract\n",
    "In this notebook we format experimental data into proper `Pandas.DataFrame` to analyse them and compare them with simulation results."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Setup\n",
    "\n",
    "In this section:\n",
    "* We define all the needed libraries.\n",
    "* We import the raw simulation results as well as the experimental measurements (that have already been formatted within the `experiment_seed_growth_tracking_analysis.ipynb` notebook).\n",
    "* We also define the folder where all the generated graphs will be recorded."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": true
   },
   "source": [
    "### Dependencies"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "import os\n",
    "\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import seaborn as sb\n",
    "import matplotlib as mpl\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "import matplotlib.gridspec as gs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### I/O Directories"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "curr_work_dir = os.getcwd()\n",
    "root_dir = curr_work_dir[:-15]\n",
    "\n",
    "# -- Where the raw experimental measurements are stored\n",
    "raw_data_dir = root_dir+'data/experimental_measurements/growth_tracking/'\n",
    "\n",
    "# -- Where the results (DataFrames in hdf5 format) will be recorded\n",
    "res_dir = root_dir+'data/analysis_results/'\n",
    "\n",
    "# -- Where the figures (in pdf format) will be recorded\n",
    "fig_dir = root_dir+'doc/figures/'\n",
    "\n",
    "# -- Checking if output directories exist and create them if not\n",
    "for path in [res_dir, fig_dir]:\n",
    "    if not os.path.isdir(path):\n",
    "        os.makedirs(path)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Name of the file containing the raw data (seed cross section measurements):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "raw_data_file_names = ['Col0-iku2_1.txt',\n",
    "                       'Col0-iku2_2.txt',\n",
    "                       'Col0-iku2_3.txt',\n",
    "                       'Col0-iku2_4.txt',\n",
    "                       'Col0-ede13_1.txt',\n",
    "                       'Col0-ede13_2.txt']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Data imports"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "exp_data = pd.DataFrame()\n",
    "for nbr, raw_data_file_name in enumerate(raw_data_file_names):\n",
    "    \n",
    "    # -- Import only necessary data\n",
    "    raw_data_path = raw_data_dir + raw_data_file_name\n",
    "    raw_data = pd.read_csv(raw_data_path, sep=\"\\t\")[['Genotype', 'DAP', 'Area']]\n",
    "    \n",
    "    # -- Add a unique identifyer to each dataset\n",
    "    raw_data['Replicate'] = nbr\n",
    "    \n",
    "    exp_data = pd.concat([exp_data, raw_data])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Pre-process\n",
    "\n",
    "In this section:\n",
    "* We will adjust a bit the **data** `DataFrame` (change some column names, compute derived properties from the ones initialy measured, trim the data to the first 10 days)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Reset the `DataFrame` index values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "exp_data = exp_data.reset_index(drop=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Useful short-cuts."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tmin = exp_data['DAP']==0\n",
    "early = exp_data['DAP']<11"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Remove time step over 10 (present in one data set)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "exp_data = exp_data[early]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Rename time variable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "exp_data = exp_data.rename(columns={'DAP':'Time (DAP)'})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Scale the Surface area variable to ease its visual representation latter on."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "exp_data['Seed area'] = exp_data['Area'] / 100000"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Compute relative mean radius from cross section surface area."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "exp_data['Relative Area'] = exp_data['Area'] / exp_data.loc[tmin, 'Area'].mean()\n",
    "exp_data['Relative Radius'] = exp_data['Relative Area'].apply(np.sqrt)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Analysis\n",
    "\n",
    "In this section:\n",
    "* We compute the **Relative Radius** values for each genotype, each replicate and each time step.\n",
    "* We also display the number of cells for each measurements.\n",
    "* Finally we estimate the initial value for the radius/thickness ratio from experimental measurements performed on three seeds at 0 DAP.\n",
    "* We compute the growth rate for each replicate and each genotype.\n",
    "* We save the polished experimental measurement as a `DataFrame` named **exp_results_all.hdf5** recorded in the `/data/exp_measures/` folder.\n",
    "* We save separately the computed growth rates into another `DataFrame`, named **exp_results_all_growth_rate.hdf5** and located within the same folder (`/data/exp_measures/`)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Getting some quantitative info from experimental data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's first have a look at the final **Relative Radius** value in details. For all the genotypes and the corresponding replicates."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for name in exp_data['Genotype'].unique():\n",
    "    geno = exp_data['Genotype']==name\n",
    "    sub_data = exp_data[geno]\n",
    "    \n",
    "    tmax = sub_data['Time (DAP)']==sub_data['Time (DAP)'].max()\n",
    "    \n",
    "    mn = sub_data.loc[tmax, 'Relative Radius'].mean()\n",
    "    sd = sub_data.loc[tmax, 'Relative Radius'].std()\n",
    "    \n",
    "    print(f\"Final relative radius value for {name}: {mn:.2f} +/- {sd:.2f}\\n\")\n",
    "    \n",
    "    print(\"=== Detailed by replicate ===\")\n",
    "    \n",
    "    for idx in sub_data['Replicate'].unique():\n",
    "        repli = sub_data['Replicate']==idx\n",
    "        \n",
    "        # -- Some replicate do not have results for 'Time (DAP)'= 10\n",
    "        final_time_replicate = sub_data[repli]['Time (DAP)'].max()\n",
    "        final_step = sub_data[repli]['Time (DAP)']==final_time_replicate\n",
    "        \n",
    "        mn = sub_data[final_step & repli]['Relative Radius'].mean()\n",
    "        sd = sub_data[final_step & repli]['Relative Radius'].std()\n",
    "        print(f\"    * Replicate {idx} ({name}): {mn:.2f} +/- {sd:.2f} at final time: {final_time_replicate}\")\n",
    "    print('\\n')\n",
    "        "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    ">**Notes:** At this point, we see that *Col-0* and *Ede1-3* have the same final size statistically and that *iku2* mutants feature significantly smaller final sizes."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's also check how much seeds have been measured for each genotype, each replicate and each time step."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "col0 = exp_data['Genotype'] == 'Col-0'\n",
    "\n",
    "for name in exp_data['Genotype'].unique():\n",
    "    print(f\"=== {name} ===\")\n",
    "    \n",
    "    geno = exp_data['Genotype'] == name\n",
    "    \n",
    "    for rep_idx in exp_data[geno]['Replicate'].unique():\n",
    "        print(f\"- Replicate number: {rep_idx}:\")\n",
    "        \n",
    "        repl = exp_data['Replicate'] == rep_idx\n",
    "        \n",
    "        seed_number = [len(exp_data[geno & repl & (exp_data['Time (DAP)'] == t)])\n",
    "                       for t in exp_data['Time (DAP)'].unique()]\n",
    "        \n",
    "        print(f\"Number of seeds cleared per day: {seed_number}\\n\")\n",
    "    print('\\n')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Measurements of seed size and seed coat thickness at 0 DAP"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def compute_radius(area):\n",
    "    \"\"\"Derives the radius of a circle from its area.\n",
    "    \"\"\"\n",
    "    return np.sqrt(area/np.pi)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From images provided by Audrey & Benoit we can estimate the seed coat thickness (see `/data/exp_measures/testa_thickness_measure.png`):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "seed_surf_area = [4500, 4000, 3500] # in micron^2\n",
    "endo_surf_area = [700, 500, 500] # in micron^2\n",
    "\n",
    "seed_rad = list(map(lambda s: compute_radius(s), seed_surf_area))\n",
    "endo_rad = list(map(lambda s: compute_radius(s), endo_surf_area))\n",
    "\n",
    "seed_rad = np.array(seed_rad)\n",
    "endo_rad = np.array(endo_rad)\n",
    "\n",
    "thickness = [sr - er for sr, er in zip(seed_rad, endo_rad)]\n",
    "thickness = np.array(thickness)\n",
    "\n",
    "rad_mn = seed_rad.mean()\n",
    "rad_sd = seed_rad.std()\n",
    "thk_mn = thickness.mean()\n",
    "thk_sd = thickness.std()\n",
    "\n",
    "rrd_mean = (seed_rad/thickness).mean()\n",
    "rrd_std = (seed_rad/thickness).std()\n",
    "\n",
    "print(f'mean seed coat thickness: {thk_mn:.2f} +- {thk_sd:.2f} micron')\n",
    "print(f'mean seed radius: {rad_mn:.2f} +- {rad_sd:.2f} micron')\n",
    "print(f'mean relative radius: {rrd_mean:.2f} +- {rrd_std:.2f}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Note:** Based on this quick minimalist estimation, we will consider an initial value of the **Relative Radius** variable of 1.5."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Computing the relative growth rate"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "growth_rate = pd.DataFrame(columns=('Replicate', 'Genotype', 'Time (DAP)', 'Relative Growth Rate'))\n",
    "\n",
    "for sim_idx in exp_data['Replicate'].unique():\n",
    "    repli = exp_data['Replicate']==sim_idx\n",
    "    \n",
    "    for genotype in exp_data.loc[repli, 'Genotype'].unique():\n",
    "\n",
    "        geno = exp_data['Genotype']==genotype\n",
    "        \n",
    "        growth_curve = exp_data[repli & geno]\n",
    "\n",
    "        time = sorted(growth_curve['Time (DAP)'].unique())[1:]\n",
    "        \n",
    "        grth_rate = []\n",
    "        grth_times = []\n",
    "        for t in time:\n",
    "            current_time = growth_curve['Time (DAP)']==t\n",
    "            previous_time = growth_curve['Time (DAP)']==t-1\n",
    "\n",
    "            x = growth_curve[current_time]['Relative Radius'].values\n",
    "            y = growth_curve[previous_time]['Relative Radius'].values\n",
    "\n",
    "            gr_mn = x.mean()/y.mean() - 1\n",
    "            \n",
    "            x2 = x**2\n",
    "            inv_y = np.array(list(map(lambda t: 1/t, y)))\n",
    "            inv_y2 = inv_y**2\n",
    "            \n",
    "            gr_std = np.sqrt(x2.mean()*inv_y2.mean()\n",
    "                             - (x.mean()*inv_y.mean())**2)\n",
    "            \n",
    "            grth_rate.append(gr_mn)\n",
    "            grth_times.append(t)\n",
    "            \n",
    "            grth_rate.append(gr_mn+gr_std)\n",
    "            grth_times.append(t)\n",
    "            \n",
    "            grth_rate.append(gr_mn-gr_std)\n",
    "            grth_times.append(t)\n",
    "        \n",
    "        res = pd.DataFrame({'Replicate': sim_idx,\n",
    "                            'Genotype': genotype,\n",
    "                            'Time (DAP)': grth_times,\n",
    "                            'Relative Growth Rate': grth_rate})\n",
    "        \n",
    "        growth_rate = pd.concat([growth_rate, res], ignore_index=True)\n",
    "        \n",
    "growth_rate['Replicate'] = growth_rate['Replicate'].astype(int)\n",
    "growth_rate['Time (DAP)'] = growth_rate['Time (DAP)'].astype(int)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    ">**Note:** To compute the standard deviation of the consecutive radii ratio, we assumed that they were independent from one another and follow gaussian distribution. This makes sense for each measurement is performed on a unique seed."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Counting the number of seeds per replicates"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A quick routine to assert the number of seeds in each replicate of the WT plants for each time step."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "wt = exp_data['Genotype'] == 'Col-0'\n",
    "\n",
    "time_steps = exp_data['Time (DAP)'].unique()\n",
    "time_steps.sort()\n",
    "\n",
    "replicates = exp_data[wt]['Replicate'].unique()\n",
    "\n",
    "for t in time_steps:\n",
    "    now = exp_data['Time (DAP)'] == t\n",
    "    \n",
    "    for r in replicates:\n",
    "        rep = exp_data['Replicate'] == r\n",
    "        nbr = exp_data[wt & now & rep]['Area'].count()\n",
    "        print(f'{nbr} seeds in replicate {r} at time step #{t}')\n",
    "    \n",
    "    print('---')\n",
    "    nbr = exp_data[wt & now]['Area'].count()\n",
    "    print(f'{nbr} seeds in all replicates at time step #{t}')\n",
    "    \n",
    "    print('\\n')\n",
    "    print('=====')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Saving results"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Save the trimed and combined `DataFrame` into a single file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "saving_name = 'exp_growth_tracking_results.hdf5'\n",
    "\n",
    "exp_data.to_hdf(raw_data_dir[:-13] + saving_name,\n",
    "                key=saving_name[:-5],\n",
    "                mode='w')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Recording growth rate into the experimental measure `DataFrame`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "saving_name = \"exp_growth_tracking_results_growth_rate.hdf5\"\n",
    "\n",
    "growth_rate.to_hdf(raw_data_dir[:-13] + saving_name,\n",
    "                   key=saving_name[:-5],\n",
    "                   mode='w')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Visualization\n",
    "\n",
    "In this section:\n",
    "* We compare the time evolution of the relative radius between genotypes and replicates.\n",
    "* We do the same for relative growth rate.\n",
    "* We also compare the dynamics of Col-0 & *iku2* mutants."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Plotting functions defintion"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We mostly want to visualize the time-evolution of two quantities: the **Relative Radius** and the **Relative Growth Rate** respectively from the **exp_data** `DataFrame` and the **growth_rate** `DataFrame`.\n",
    "\n",
    "We want also to be able to asset the differences between genotypes and even replicates within the same genotype (when this is possible). \n",
    "\n",
    "To that end, we divided our plotting strategy into three main functions:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**First, a *core* function**, that generates plots given a property name, a genotype and a `DataFrame`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "code_folding": [
     0
    ]
   },
   "outputs": [],
   "source": [
    "def plot_property(prop_name, genotype, data, ax, legend=False, avg=False, violin=False):\n",
    "    \"\"\"Plots a property from a DataFrame.\n",
    "    \n",
    "    Parameters\n",
    "    ----------\n",
    "    prop_name: str\n",
    "        Name of the property to plot.\n",
    "        Should be a column of the considered DataFrame.\n",
    "    genotype: str\n",
    "        Should be in ['Col-0', 'iku2', 'ede1-3']\n",
    "    data: Pandas.DataFrame\n",
    "        Should be either exp_data or growth_rate\n",
    "    ax: Matplotlib.Axe\n",
    "        Where to draw the graph.\n",
    "    legend: bool or string\n",
    "        Optional (Default: False)\n",
    "        Argument from the seaborn.lineplot class.\n",
    "    avg: Bool\n",
    "        Optional (default: False).\n",
    "        If True, only the averaged behavior of\n",
    "        all replicates is plotted.\n",
    "    violin: bool\n",
    "        Optional (default: False).\n",
    "        If True, plot a violin plot\n",
    "        of the property pooled from all replicates.\n",
    "\n",
    "    \"\"\"\n",
    "    geno = data['Genotype']==genotype\n",
    "    \n",
    "    # --\n",
    "    palette = sb.color_palette('rocket', 6)\n",
    "    \n",
    "    colors = {'Col-0': palette, \n",
    "              'iku2': palette[:4],\n",
    "              'ede1-3': palette[-2:]}\n",
    "    \n",
    "    color_avg = {'Col-0': palette[1:2], \n",
    "                 'iku2': palette[3:4],\n",
    "                  'ede1-3': palette[-2:-1]}\n",
    "    \n",
    "    marker = {'Col-0': 'o', \n",
    "              'iku2': 's',\n",
    "              'ede1-3': '^'}\n",
    "    # --\n",
    "    if avg:\n",
    "        sb.lineplot(x='Time (DAP)', y=prop_name,\n",
    "                    hue='Genotype', style='Genotype',\n",
    "                    data=data[geno],\n",
    "                    palette=color_avg[genotype], marker=marker[genotype],\n",
    "                    err_style='band', ci='sd', legend=legend, ax=ax)\n",
    "    \n",
    "    elif violin:\n",
    "        violin = sb.violinplot(x='Time (DAP)', y=prop_name,\n",
    "                               data=data, palette=color_avg[genotype], hue='Genotype',\n",
    "                               scale='count', inner=None, linewidth=0,\n",
    "                               label=genotype+' Exp. data')\n",
    "\n",
    "    else:        \n",
    "        rep_nbr = len(data.loc[geno, 'Replicate'].unique())\n",
    "        \n",
    "        sb.lineplot(x='Time (DAP)', y=prop_name,\n",
    "                        hue='Replicate', style='Replicate',\n",
    "                        data=data[geno],\n",
    "                        palette=colors[genotype], marker=marker[genotype],\n",
    "                        dashes=rep_nbr*[(3, 2)],\n",
    "                        err_style='bars', ci='sd', legend=legend, ax=ax)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Second, a *simple* function** to compare growth between phenotypes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "code_folding": []
   },
   "outputs": [],
   "source": [
    "def plot_growth(genotypes, save=False):\n",
    "    \"\"\"Plots the surface area of the seeds cross section as a function of time.\n",
    "    \n",
    "    Parameters\n",
    "    ----------\n",
    "    genotypes : str or list(str)\n",
    "        The genotype(s) to consider.\n",
    "        Should be in ['Col-0', 'iku2', 'ede1-3']\n",
    "    save: bool\n",
    "        Optional (default: False).\n",
    "        If true, the graph is recorderd.\n",
    "    \"\"\"\n",
    "    \n",
    "    if not isinstance(genotypes, list): genotypes = [genotypes]\n",
    "    geno = exp_data['Genotype'].isin(genotypes)\n",
    "    \n",
    "    # --\n",
    "    palette = sb.color_palette('rocket', 4)[-2:]\n",
    "    \n",
    "    # --\n",
    "    sb.set(context='talk', style='white')\n",
    "\n",
    "    fig = plt.figure(figsize=(11, 7))\n",
    "    grd = gs.GridSpec(1,1)\n",
    "    axe = fig.add_subplot(grd[0,0])\n",
    "\n",
    "    # --\n",
    "    \n",
    "    ax = sb.violinplot(x='Time (DAP)', y='Seed area',\n",
    "                  data=exp_data[geno], palette=palette, hue='Genotype',\n",
    "                  scale='count', inner='points', linewidth=.2)\n",
    "    plt.setp(ax.collections, edgecolor=\"k\")\n",
    "    \n",
    "    # annotate the violin plot\n",
    "    for t in exp_data['Time (DAP)'].unique():\n",
    "\n",
    "        time = exp_data['Time (DAP)']==t\n",
    "        if t == 1:\n",
    "            star = '**'\n",
    "        else:\n",
    "            star = '****'\n",
    "\n",
    "        r = exp_data.loc[time & geno, 'Seed area'].max()\n",
    "        r += .1\n",
    "\n",
    "        ax.annotate(star, (t, r), \n",
    "                    fontsize= 20,\n",
    "                    color='k', #color_geno[geno][-1],\n",
    "                    horizontalalignment='center')\n",
    "        \n",
    "    # --\n",
    "    plt.ylabel('Seed area ($10^5\\mu m^2$)')\n",
    "    plt.xlabel('Time (DPA)')\n",
    "    sb.despine(trim=True)\n",
    "    plt.tight_layout()\n",
    "    \n",
    "    # --\n",
    "    if save:\n",
    "\n",
    "        fig_name = '_'.join('Seed area'.split())\n",
    "        fig_name += '_vs_time_expMeasures_'\n",
    "        fig_name += '_'.join(genotypes)\n",
    "        fig_name += '.pdf'\n",
    "        \n",
    "        fig.savefig(fig_dir+fig_name, dpi=300)\n",
    "        \n",
    "        print(f'Saved graph at location: {fig_dir}, under name: {fig_name}')\n",
    "    \n",
    "    plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Third, another *wrapping* function** that focus on displaying both properties (*i.e.* **Relative Radius** & **Relative Growth Rate**) together."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "code_folding": [
     0
    ]
   },
   "outputs": [],
   "source": [
    "def plot_radius_and_growth_rate(genotypes, save=False, avg=False):\n",
    "    \"\"\"Plots side by side radius and growth rate time evolution.\n",
    "    \n",
    "    Parameters\n",
    "    ----------\n",
    "    genotypes : str or list(str)\n",
    "        The genotype(s) to consider.\n",
    "        Should be in ['Col-0', 'iku2', 'ede1-3']\n",
    "    save: bool\n",
    "        Optional (default: False).\n",
    "        If true, the graph is recorderd.\n",
    "    avg: Bool\n",
    "        Optional (default: False).\n",
    "        If True, only the averaged behavior of\n",
    "        all replicates is plotted.\n",
    "\n",
    "    \"\"\"\n",
    "    if not isinstance(genotypes, list): genotypes = [genotypes]\n",
    "        \n",
    "    # --\n",
    "    if avg:\n",
    "        height = 1\n",
    "        title = [None, None, None]\n",
    "    else:\n",
    "        height = len(genotypes)\n",
    "        title = [genotype+'\\n'+'Replicates' for genotype in genotypes]\n",
    "    \n",
    "    # --\n",
    "    fig = plt.figure(figsize=(15, 5*height))\n",
    "    grd = gs.GridSpec(height, 2)\n",
    "    \n",
    "    if len(genotypes) > 1 and not avg:\n",
    "        axe = [fig.add_subplot(grd[i//2, i%2])\n",
    "               for i in range(2*len(genotypes))]\n",
    "    else:\n",
    "        axe = [fig.add_subplot(grd[:, 0]),\n",
    "               fig.add_subplot(grd[:, -1])]\n",
    "\n",
    "    # --\n",
    "    for i, genotype in enumerate(genotypes):\n",
    "\n",
    "        if 2*i >= len(axe): i = 0\n",
    "\n",
    "        plot_property('Relative Radius', genotype, exp_data,\n",
    "                      axe[2*i], avg=avg)\n",
    "        \n",
    "        plot_property('Relative Growth Rate', genotype, growth_rate,\n",
    "                      axe[2*i+1], legend='full', avg=avg)\n",
    "\n",
    "    # --\n",
    "    for i, ax in enumerate(axe):\n",
    "        if i%2 == 1:\n",
    "            ax.legend(loc='upper right',\n",
    "                      bbox_to_anchor=(1.3, .8),\n",
    "                      title=title[i//2])\n",
    "\n",
    "    sb.despine(trim=True)\n",
    "    plt.tight_layout()\n",
    "    \n",
    "    # --\n",
    "    if save:\n",
    "\n",
    "        fig_name = 'RelativeRadius_and_GrowthRate_vs_time_expMeasures_'\n",
    "        fig_name += '_'.join(genotypes)\n",
    "        if avg: fig_name += '_averaged'\n",
    "        fig_name += '.pdf'\n",
    "        \n",
    "        fig.savefig(fig_dir+fig_name, dpi=300)\n",
    "        \n",
    "        print(f'Saved graph at location: {fig_dir}, under name: {fig_name}')\n",
    "    \n",
    "    plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Surface area versus time (Fig.2c)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_growth(['Col-0', 'iku2'], save=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Relative radius and relative growth rate versus time in Col-0 (Supp.Fig.1b,c)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_radius_and_growth_rate('Col-0', save=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Relative growth comparison between Col-0 & *iku2* (Supp. Fig.5a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Note:** Only the right panel of the figure below has been used in Supp.Fig.5a."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_radius_and_growth_rate(['Col-0', 'iku2'], save=True, avg=True)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3.8.13 ('seed')",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.13"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": false,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  },
  "vscode": {
   "interpreter": {
    "hash": "a03340c5f2b18573e3baecfb58e1a3ba2c99cc2b4589f9ac3464355dfbcc1598"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
