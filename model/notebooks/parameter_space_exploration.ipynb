{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Parameter space exploration\n",
    "\n",
    "\n",
    "### Abstract\n",
    "In this notebook we will analyze the results generated by the parameter space exploration of our model. To that end, the main question we will address is the following: *Is there a region of the considered parameter space where the model generate growth patterns comparable to the ones measured experimentally?*\n",
    "\n",
    "To address this question, we will:\n",
    "* Trim our simulation results in order to only keep the ones converging toward realistic values (*i.e.* in arcordance with experimental measurements).\n",
    "* Format the simulation and experimental data so they can be compared with each other.\n",
    "* Define a metric to compare experiments and simulations and apply this metric to sort our simulations based on their *matching degree* with experimental measurements."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Setup\n",
    "\n",
    "This this section:\n",
    "* We define all the needed libraries.\n",
    "* We import the raw simulation results as well as the experimental measurements (that have already been formatted within the `experiment_seed_growth_tracking_analysis.ipynb` notebook).\n",
    "* We also define the folder where all the generated graphs will be recorded."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": true
   },
   "source": [
    "### Dependencies"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "import os\n",
    "\n",
    "import pandas as pd\n",
    "import numpy as np\n",
    "import seaborn as sb\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "import matplotlib.gridspec as gs\n",
    "\n",
    "from collections.abc import Iterable\n",
    "from numpy import linalg as lng"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": true
   },
   "source": [
    "### I/O Directories"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "curr_work_dir = os.getcwd()\n",
    "root_dir = os.getcwd()[:-15]\n",
    "\n",
    "# -- Where the experimental results (DataFrame version) are stored\n",
    "exp_res_dir = root_dir+'data/analysis_results/'\n",
    "\n",
    "# -- Where the data generated by the parameter space exploration are stored\n",
    "sim_res_dir = root_dir+'data/data_param_space_explo/'\n",
    "\n",
    "# -- Where the results (DataFrames in hdf5 format) will be recorded\n",
    "res_dir = root_dir+'data/analysis_results/'\n",
    "\n",
    "# -- Where the figures (in pdf format) will be recorded\n",
    "fig_dir = root_dir+'doc/figures/'\n",
    "\n",
    "# -- Checking if output directories exist and create them if not\n",
    "for dir_path in [res_dir, fig_dir]:\n",
    "    if not os.path.isdir(dir_path):\n",
    "        os.makedirs(dir_path)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Data imports"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Simulation raw data "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We import the data generated by the `pypet` library used to perform the parameter-space exploration.\n",
    "\n",
    "We are going to use this procedure for two data sets. To choose between both, we use the variable `pressure_function`:\n",
    "* if `pressure_function == 'Cst'` we will consider the first data set, generated with constant pressure.\n",
    "* if `pressure_function == 'Var'` we will consider the second data set, generated with a tim-dependent pressure function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pressure_function = 'Var' # 'Cst' or 'Var'\n",
    "\n",
    "if pressure_function == 'Cst':\n",
    "    folder_names = ['210212_124047_seed_growth/hdf5/', \n",
    "                    '210212_133507_seed_growth/hdf5/',\n",
    "                    '210212_135516_seed_growth/hdf5/',\n",
    "                    '210212_141814_seed_growth/hdf5/']\n",
    "\n",
    "elif pressure_function == 'Var':\n",
    "    folder_names = ['220705_123626_seed_growth_pvar/hdf5/', \n",
    "                    '220705_123639_seed_growth_pvar/hdf5/',\n",
    "                    '220705_123646_seed_growth_pvar/hdf5/',\n",
    "                    '220705_123653_seed_growth_pvar/hdf5/']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Note:** If using your own generated data for the analysis, please change the names of the folders above accordingly."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "raw_sim_list = []\n",
    "for folder_name in folder_names:\n",
    "    path_to_file = sim_res_dir + folder_name + 'formatted_dataframe.hdf5'\n",
    "\n",
    "    raw_sim_list.append(pd.read_hdf(path_to_file, 'df'))\n",
    "\n",
    "sim_raw = pd.concat(raw_sim_list, ignore_index=True)\n",
    "\n",
    "# -- Display some info\n",
    "\n",
    "print(\"Columns within the dataframe:\")\n",
    "for column in sim_raw.columns:\n",
    "    print(f\"  *  {column}\")\n",
    "\n",
    "print(\"-----\")\n",
    "print(\"Parameter ranges:\")\n",
    "params = ['pressure', 'alpha', 'gamma', 'rho', 'eta']\n",
    "for param in params:\n",
    "    print(f'  *  {param} betweeen {min(sim_raw[param]):.1f} and '\n",
    "          + f'{max(sim_raw[param]):.1f}'\n",
    "          + f', {len(set(sim_raw[param].values))} increments')\n",
    "\n",
    "nbr_runs = len(sim_raw)\n",
    "time_steps = sim_raw['time'][0]\n",
    "time_step_nbr = len(time_steps)\n",
    "print(\"-----\")\n",
    "print(f\"Total number of simulations: {nbr_runs:.2e}\")\n",
    "print(f\"Number of time steps per the simulation: {time_step_nbr}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's now add a useful variable to the `DataFrame`: the ratio between the initial and final radius for each simulation.\n",
    "\n",
    "We will also check if, for some reason, simulations did not run properly. To that end, we assess that the length of each simulation result matches the number of time steps. If not, the corresponding simulation will be labelled with a *radratio* of $-1$, so they can be discharged latter on. We reffer to these *wrong* simulations as *diverging trajectories* (in the state space)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sim_raw['radratio'] = [np.mean(rads[101:111]) / rads[0]\n",
    "                       if (len(rads) == 150) & (np.abs(np.diff(rads[-2:])[0]/rads[-2]) < .01)\n",
    "                       else -1\n",
    "                       for rads in sim_raw['radius']]\n",
    "\n",
    "nmbr_dvrg_traj = 0\n",
    "for idx, row in sim_raw.iterrows():\n",
    "    if row['radratio']==-1:\n",
    "        nmbr_dvrg_traj += 1\n",
    "\n",
    "# -- let's also number our simulations\n",
    "sim_raw['Replicate'] = sim_raw.index\n",
    "\n",
    "# -- \n",
    "\n",
    "print(f\"Number of diverging trajectories: {nmbr_dvrg_traj}\")\n",
    "print(f\"Number of bounded trajectories: {nbr_runs - nmbr_dvrg_traj}\")\n",
    "print(f\"Percentage of bounded traj: {(1 - nmbr_dvrg_traj/nbr_runs):.2%}\")\n",
    "print(f\"Final radius ration between {min(sim_raw['radratio'])} and {max(sim_raw['radratio'])}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Experimental data "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We are going to need these data latter to perform the fit and the analysis."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "exp_data_name = \"exp_growth_tracking_results.hdf5\"\n",
    "exp_res_path = res_dir + exp_data_name\n",
    "\n",
    "exp_res = pd.read_hdf(exp_res_path, exp_data_name[:-5])\n",
    "\n",
    "# -- Defining sub-groups based on the seed genotypes\n",
    "gen = {geno: exp_res['Genotype']==geno\n",
    "       for geno in exp_res['Genotype'].unique()}\n",
    "\n",
    "col = exp_res['Genotype']=='Col-0'\n",
    "iku =  exp_res['Genotype']=='iku2'\n",
    "ede = exp_res['Genotype']=='ede1-3'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": true
   },
   "source": [
    "## Pre-process\n",
    "\n",
    "This this section:\n",
    "* We define a new `DataFrame` containing only simulations converging toward *realistic* values.\n",
    "* We will discard some useless properties recorded during the parameter space exploration.\n",
    "* We also modify a bit the format of this `DataFrame` by flattening the arrays that it may contain."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "### Trim"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "Experimental measurements provide us with ranges for the steady state value of the seed radius:\n",
    "$$\\frac{r_{\\infty}}{r_0} = \n",
    "\\begin{cases}\n",
    "3.80 \\pm 0.26 & \\text{wt} \\\\\n",
    "3.27 \\pm 0.30 & \\text{iku2} \n",
    "\\end{cases}$$\n",
    "\n",
    "> **Note:** See notebook `experiment_seed_growth_tracking_analysis.ipynb`.\n",
    "\n",
    "The goal here being to filter out unrelevant simulations, we are going to set a rather large condition that will encompass both *WT* and *iku2* experimental values. To that end, we are going to consider the following condition:\n",
    "\n",
    "$$\n",
    "\\frac{r_{\\infty}}{r_0} = 3.5 \\pm 0.5\n",
    "$$\n",
    "\n",
    "This value intervalle has also to be reached in a realist time. In experiments, the steady state is roughly reached in less than 10 DAP (Days After Polination). We are going to use that as well to narrow down the set of relevant simulations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "code_folding": [],
    "hidden": true
   },
   "outputs": [],
   "source": [
    "min_radratio = 3.0\n",
    "max_radratio = 4.0\n",
    "\n",
    "select = ((min_radratio < sim_raw['radratio']) &\n",
    "          (sim_raw['radratio']< max_radratio))\n",
    "\n",
    "sim_kept = sim_raw.loc[select, ['Replicate',\n",
    "                                'pressure',\n",
    "                                'alpha',\n",
    "                                'gamma',\n",
    "                                'rho',\n",
    "                                'eta',\n",
    "                                'time',\n",
    "                                'radius',\n",
    "                                'stiffness',\n",
    "                                'radratio']].copy()\n",
    "\n",
    "# -- Display some info\n",
    "print(f\"Interesting runs: {len(sim_kept)} over {len(sim_raw):.2e} total runs (i.e. {len(sim_kept)/len(sim_raw):.2%})\")\n",
    "\n",
    "for param in ['alpha', 'gamma', 'rho', 'eta']:\n",
    "    vmin = round(min(sim_kept[param].values), 1)\n",
    "    vmax = round(max(sim_kept[param].values), 1)\n",
    "    print(\"%s betwenne %s and %s\" %(param, vmin, vmax))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "The **sim_kept** `DataFrame` contains only the simulations that converge toward a radius value contained within the experimental value range."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "### Format"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "We need now to flatten the **sim_kept** `DataFrame` (*i.e.* remove the arrays in it) so we can make use of all the `Pandas` library tools properly."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "sim_res = pd.DataFrame()\n",
    "\n",
    "for i, (sim_idx, pressure, alpha, gamma, rho, eta,\n",
    "          time, radius, stiffness, final_radius_ratio) in sim_kept.iterrows():\n",
    "\n",
    "    one_sim = pd.DataFrame({'Replicate': sim_idx,\n",
    "                            'alpha': alpha,\n",
    "                            'gamma': gamma,\n",
    "                            'rho': rho,\n",
    "                            'eta': eta,\n",
    "                            'Pressure': pressure,\n",
    "                            'Time (A.U.)': time[:111],\n",
    "                            'Time (DAP)': list(map(lambda t: round(5*t), time[:111])),\n",
    "                            'Radius (A.U.)': radius[:111],\n",
    "                            'Stiffness (A.U.)': stiffness[:111],\n",
    "                            'Relative Radius': radius[:111]/radius[0],\n",
    "                            'Final Radius Ratio': final_radius_ratio})\n",
    "    \n",
    "    sim_res = pd.concat([sim_res, one_sim], ignore_index=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": true
   },
   "source": [
    "## Analysis \n",
    "\n",
    "In this section:\n",
    "* We compute the *matching score* between all the simulations contained with the **sim_res** `DataFrame` and the experimental measurements contained within the **exp_res** `DataFrame`.\n",
    "* We save a version of the **sim_res** `DataFrame` updated with the results of this comparison (*i.e.* matching score and rank added for each simulation)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "### Fit simulations with experiments"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "We first sample each simulation within the **sim_res** `DataFrame` at time steps corresponding to the experimental measurements."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "time = exp_res['Time (DAP)'].unique()\n",
    "\n",
    "mean_sim_rad = {sim_idx: np.array([sim_res.loc[(sim_res['Replicate']==sim_idx) &\n",
    "                                               (sim_res['Time (DAP)']==t),\n",
    "                                               'Relative Radius'].mean()\n",
    "                                   for t in time])\n",
    "                for sim_idx in sim_res['Replicate'].unique()}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "Then, for each genotype (*i.e.* **Col-0**, **iku2** and **ede1-3**) we compare all simulations (within the **sim_res** `DataFrame`) with the corresponding measurements. To that end, we use the classic **root mean square distance**:\n",
    "\n",
    "$$\n",
    "\\text{dist}(\\boldsymbol{s}, \\boldsymbol{e}) = \\sqrt{\\sum_k (s_k - e_k)^2},\n",
    "$$\n",
    "\n",
    "where the vector $\\boldsymbol{s}$ depicts the relative radius value of a given simulation sampled on the experimental time points and $\\boldsymbol{e}$ the average pf experimental measurements (radius normalized by its initial mean value) for a given genotype.\n",
    "\n",
    "We use the inverse of this distance as a fitting score to rank simulations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "for geno in ['Col-0', 'iku2', 'ede1-3', 'mixed']:#exp_res['Genotype'].unique()\n",
    "    \n",
    "    # -- Selecting the proper data\n",
    "    if geno != 'mixed':\n",
    "        selection = (exp_res['Genotype']==geno)\n",
    "    else:\n",
    "        selection = exp_res['Genotype'].isin(['Col-0', 'iku2'])\n",
    "\n",
    "    mean_exp_rad = np.array([exp_res.loc[selection & (exp_res['Time (DAP)']==t),\n",
    "                                         'Relative Radius'].mean()\n",
    "                             for t in time])\n",
    "    \n",
    "\n",
    "    # -- Root mean square distance computation\n",
    "    dist_sim_exp = {sim_idx: lng.norm(mean_rad - mean_exp_rad)\n",
    "                    for sim_idx, mean_rad in mean_sim_rad.items()}\n",
    "\n",
    "    # -- Getting the sim index sorted by fitting score \n",
    "    #    N.B.: complex loop due to different simulation with the same fitting score.\n",
    "    all_values = list(dist_sim_exp.values())\n",
    "    all_sim_indices = list(dist_sim_exp.keys())\n",
    "\n",
    "    sorted_sim_indices = []\n",
    "    while len(all_values) > 0:\n",
    "        val = list(np.sort(all_values)).pop(0)\n",
    "        kdx = all_values.index(val)\n",
    "        all_values.pop(kdx)\n",
    "        sim_idx = all_sim_indices.pop(kdx)\n",
    "        sorted_sim_indices.append(sim_idx)\n",
    "\n",
    "    # -- Adding the fitting rank and score to the dataframe\n",
    "    sim_res[geno + ' Fitting Rank'] = sim_res['Replicate'].apply(lambda x: sorted_sim_indices.index(x))\n",
    "    sim_res[geno + ' Fitting Score'] = sim_res['Replicate'].apply(lambda x: min(list(dist_sim_exp.values()))/dist_sim_exp[x])\n",
    "    sim_res[geno + ' Fitting Score (Raw)'] = sim_res['Replicate'].apply(lambda x: 1/dist_sim_exp[x])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "> **Note:** We consider two versions of the *fitting score*:\n",
    "> * A *raw* value which is simply the inverse of the root mean square distance with experimental measurements.\n",
    "> * A *normalized* version which corresponds, for each genotype considered, to the *raw* value divided by its best value, so it is normalized to 1."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "#### Comparison with theoretical expectations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "In the `steady_state_analysis.ipynb` notebook we plotted the roots ($I_0, I_1, I_2$) of our *ODEs* system as functions of the parameters $\\alpha, \\rho \\& \\eta$. This enabled us to define regions of the parameter space that produce stable stationnary solutions.\n",
    "\n",
    "Let's check now if the best-fitting simulation obtained through our parameter space exploration is, indeed, within these regions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "best = sim_res['Col-0 Fitting Rank'] == sim_res['Col-0 Fitting Rank'].min()\n",
    "# best = sim_res['mixed Fitting Rank'] == sim_res['mixed Fitting Rank'].min()\n",
    "final = sim_res['Time (DAP)'] == sim_res['Time (DAP)'].max()\n",
    "\n",
    "final_state = sim_res.loc[best & final]\n",
    "\n",
    "for param in ['alpha', 'rho', 'eta', 'gamma', 'Relative Radius', 'Replicate']:\n",
    "    \n",
    "    print(f'{param} = {final_state[param].unique()[0]:.2f}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "**N.B.:** These values will be used within the `steady_state_analysis.ipynb` as reference values."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "#### Parameters statistics:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "genotypes = ['Col-0', 'iku2', 'ede1-3', 'mixed'] #exp_res['Genotype'].unique()\n",
    "\n",
    "best_fit = {geno: sim_res[geno+' Fitting Rank']==0 for geno in genotypes}\n",
    "\n",
    "best_fit_100 = {geno: sim_res[geno+' Fitting Rank'].isin(np.arange(100))\n",
    "                for geno in genotypes}\n",
    "\n",
    "best_fit_20prcnt = {geno: sim_res[geno+' Fitting Score'] >=.8\n",
    "                    for geno in genotypes}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "Let's first look at the parameters of the best fitting simulations:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "code_folding": [],
    "hidden": true
   },
   "outputs": [],
   "source": [
    "for geno, slct in best_fit.items():\n",
    "    print(f'Parameter values for {geno} best fitting simulation:')\n",
    "    \n",
    "    score = sim_res.loc[slct, geno + ' Fitting Score (Raw)'].unique()[0]\n",
    "    sim_idx = sim_res.loc[slct, 'Replicate'].unique()[0]\n",
    "    \n",
    "    print(f'    * Score (absolute value): {score:.2f}')\n",
    "    print(f'    * Sim. #: {sim_idx}')\n",
    "    print('    * Parameter values:')\n",
    "    \n",
    "    for name in ['alpha', 'eta', 'gamma', 'rho']:\n",
    "        val = sim_res.loc[slct, name].unique()[0]\n",
    "        \n",
    "        print(f'        - {name}: {val:.2f}')\n",
    "    \n",
    "    print('\\n')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "Then, let's widen our analysis to the 100 simulations closest to the experimental measurements:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "for geno, slct in best_fit_100.items():\n",
    "    print(f'Parameter values for {geno} 100 best fitting simulations:')\n",
    "    for name in ['alpha', 'eta', 'gamma', 'rho']:\n",
    "        vals = sim_res.loc[slct, name].to_numpy()\n",
    "        mn_val = vals.mean()\n",
    "        md_val = np.median(vals)\n",
    "        sd_val = vals.std()\n",
    "\n",
    "        print(f'    * {name}:')\n",
    "        print(f'        - mean = {mn_val:.2f}')\n",
    "        print(f'        - median = {md_val:.2f} ')\n",
    "        print(f'        - st dev = {sd_val:.2f}')\n",
    "    print('\\n')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "### Recording results"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "if pressure_function == 'Cst':\n",
    "    saving_name = \"sim_res_cstePressure.hdf5\"\n",
    "\n",
    "elif pressure_function == 'Var':\n",
    "    saving_name = \"sim_res_pressureDrop_pieceWiseLinear_iku2_mergedAllReplicates.hdf5\"\n",
    "\n",
    "saving_path = res_dir + saving_name\n",
    "\n",
    "sim_res.to_hdf(saving_path,\n",
    "               key=saving_name[:-5],\n",
    "               mode='w')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "### Fitting scores"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": true,
    "hidden": true
   },
   "source": [
    "#### Compute fitting scores for all simulations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "We need to expand the fitting scores (and ranks) to all simulations (*i.e.* to all raws of the **sim_raw** `DataFrame`). We will give a null score to simulations that are not selected in the **sim_res** `DataFrame`.\n",
    "> **Note:** Since the `DataFrame` we are about to generate will be very big and its use restricted to the visualization of the fitting score, we choose not to include its generation within the **Analysis** section."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "Let's first define a function to expand the fitting score and rank to the whole raw data set."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "code_folding": [
     0
    ],
    "hidden": true
   },
   "outputs": [],
   "source": [
    "def compute_fitting_metric(sim_idx, genotype='Col-0', metric='Score'):\n",
    "    \"\"\"Computes a fitting score /rank for all simulations.\n",
    "    \n",
    "    Parameter\n",
    "    ---------\n",
    "    sim_idx : int.\n",
    "        The replicate number of the considered simulation.\n",
    "        Should be between 0 and 499 999.\n",
    "    genotype : str.\n",
    "        Optional (default: 'Col-0'). The Genotype to process.\n",
    "        Should be in ['Col-0', 'iku2', 'ede1-3']\n",
    "    metric : str.\n",
    "        Optional (defautl: 'Score'). Either the 'Rank' or the 'Score'.\n",
    "        \n",
    "    Returns\n",
    "    -------\n",
    "    float (if metric=='Score') or int (if metric=='Rank').\n",
    "    \n",
    "    \"\"\"\n",
    "    \n",
    "    if sim_idx in sim_res['Replicate'].unique():\n",
    "        slct = sim_res['Replicate']==sim_idx\n",
    "        return sim_res.loc[slct, genotype + ' Fitting '+ metric].unique()[0]\n",
    "    else:\n",
    "        return 0   "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "Let's now apply this function to all three genotypes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "sim_fit = sim_raw[['Replicate', 'alpha', 'gamma', 'rho', 'eta']].copy()\n",
    "\n",
    "# --\n",
    "for g in ['Col-0', 'iku2', 'ede1-3']:\n",
    "    sim_fit[g + ' Fitting Score'] = sim_fit['Replicate'].apply(lambda i: compute_fitting_metric(i, genotype=g))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Note:** The cell above might take several minutes to run depending on your computer."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": true
   },
   "source": [
    "## Visualization\n",
    "\n",
    "In this section:\n",
    "* We plot the **Relative Radius** variable from the best fitting simulations against time for the various genotypes.\n",
    "* We also propose some visualization of the fitting score over the paramter space."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": true,
    "hidden": true
   },
   "source": [
    "### Definitions of some useful functions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "Fist a function to plot the time evolution of the relative radius:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "code_folding": [],
    "hidden": true
   },
   "outputs": [],
   "source": [
    "def plot_relative_radius(genotype, best=[1, 100], violin=True, save=False):\n",
    "    \"\"\"Plots and saves graphs of relative radius vs time.\n",
    "    \n",
    "    Parameters\n",
    "    ----------\n",
    "    genotype : str\n",
    "        Should be in ['Col-0', 'iku2', 'ede1-3', 'mixed].\n",
    "    best : int or list(int)\n",
    "        Optional (default = [1, 100]).\n",
    "        if best=1 only the simulation closest to exp. data will be plotted.\n",
    "        if best=100 the 100 closest simulations will be plotted.\n",
    "        if best = [1, 100] both above cases will be plotted.\n",
    "    violin : bool\n",
    "        Optional (default = True).\n",
    "        If True a violin plot of experimental data is added for each time step.\n",
    "    save : bool\n",
    "        Optional (default = False)\n",
    "        if True save the graph.\n",
    "        \n",
    "    \"\"\"\n",
    "    if not isinstance(genotype, list): genotype = [genotype]\n",
    "    if not isinstance(best, Iterable): best = [best]\n",
    "    \n",
    "    fig = plt.figure(figsize=(22, 10))#(11, 5)\n",
    "    sb.set(context='talk', style='white')\n",
    "\n",
    "    colors = sb.color_palette('rocket_r', 10)\n",
    "    markers = {'Col-0': 'o', 'iku2': 's', 'ede1-3':'^', 'mixed': '*'}\n",
    "    \n",
    "    color_geno = {'Col-0': colors[1:2],\n",
    "                  'iku2': colors[3:4],\n",
    "                  'ede1-3': colors[5:6],\n",
    "                  'mixed': colors[7:8]}\n",
    "    \n",
    "    shift = [.75]\n",
    "    if len(genotype)>=2: shift.append(-.75)\n",
    "    if len(genotype)>=3: shift.append(-.5)\n",
    "    \n",
    "    # --\n",
    "    for geno, shft in zip(genotype, shift):\n",
    "        \n",
    "        # simulation results\n",
    "        if 1 in best:\n",
    "            sb.lineplot(x='Time (DAP)', y='Relative Radius',\n",
    "                        data=sim_res[best_fit[geno]], linestyle=':', ci=None,\n",
    "                        color=color_geno[geno][0], label='Best fit')\n",
    "\n",
    "        if 100 in best:\n",
    "            sb.lineplot(x='Time (DAP)', y='Relative Radius',\n",
    "                        data=sim_res[best_fit_100[geno]], ci='sd',\n",
    "                        color=color_geno[geno][-1], label='100 Best fits')\n",
    "        \n",
    "        # experiments results\n",
    "        if violin:\n",
    "            violin = sb.violinplot(x='Time (DAP)', y='Relative Radius',\n",
    "                                   data=exp_res[gen[geno]], color=color_geno[geno][-1],\n",
    "                                   scale='count', inner=None, linewidth=0,\n",
    "                                   label=geno+' Exp. data')\n",
    "            \n",
    "            # annotate the violin plot\n",
    "            for t in exp_res['Time (DAP)'].unique():\n",
    "\n",
    "                time = exp_res['Time (DAP)']==t\n",
    "                nbr = len(exp_res.loc[time & gen[geno]])\n",
    "\n",
    "                r = exp_res.loc[time & gen[geno], 'Relative Radius'].median()\n",
    "                r += shft\n",
    "\n",
    "                violin.annotate(str(nbr), (t, r), \n",
    "                                fontsize= 12,\n",
    "                                color=colors[-1], #color_geno[geno][-1],\n",
    "                                horizontalalignment='center')\n",
    "            \n",
    "        sb.lineplot(x='Time (DAP)', y='Relative Radius',\n",
    "                    data=exp_res[gen[geno]], marker=markers[geno], linestyle='',\n",
    "                    err_style='bars', ci='sd', color=colors[-1],\n",
    "                    label=geno+' Exp. data')\n",
    "        \n",
    "    # --\n",
    "    plt.legend(loc='lower right', bbox_to_anchor=(1.5, 0))\n",
    "    if len(genotype)>1:\n",
    "        plt.ylim([0, 5])\n",
    "    sb.despine(trim=True)\n",
    "    plt.tight_layout()\n",
    "    \n",
    "    # --\n",
    "    if save:\n",
    "        \n",
    "        ending = '_'\n",
    "        ending += ending.join(genotype)\n",
    "        if 1 in best: ending += '_bestFit'\n",
    "        if 100 in best: ending += '_100BestFit'\n",
    "            \n",
    "        fig_name = f'relatrad_vs_time_compaSimExp{ending}.pdf'\n",
    "        fig.savefig(fig_dir+fig_name, dpi=300)\n",
    "        \n",
    "        print(f'Saved graph at location: {fig_dir}, under name: {fig_name}')\n",
    "    \n",
    "    # --    \n",
    "    plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "Then, a function to plot a detailed representation of the fitting score:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot_fitting_score(genotypes, show_best_fit_position=True, save=False):\n",
    "    \"\"\"Displays visual representation of the fitting score.\n",
    "\n",
    "    Parameters\n",
    "    ----------\n",
    "    genotypes : str or list[str]\n",
    "        The name or list of names of the genotypes to visualize.\n",
    "        The names should be a subset of ['Col-0', 'iku2', 'ede1-3'].\n",
    "    show_best_fit_position : bool\n",
    "        Optional (default is True). If True, displays a star on the abscissae\n",
    "        of the best fitting simulation.\n",
    "    save : bool\n",
    "        Optional (default is False). If True, records the generated figure.\n",
    "\n",
    "    Returns\n",
    "    -------\n",
    "    None\n",
    "    \"\"\"\n",
    "\n",
    "    t0 = sim_res['Time (A.U.)']==0\n",
    "\n",
    "    if not isinstance(genotypes, list): genotypes = [genotypes]\n",
    "    nbr_rows = len(genotypes)\n",
    "\n",
    "    # --\n",
    "    sb.set(context='talk', style='ticks', font_scale=1.5)\n",
    "\n",
    "    fig = plt.figure(figsize=(22.5, 5*nbr_rows))\n",
    "    grd = gs.GridSpec(nbr_rows, 3)\n",
    "    axes = []\n",
    "    for i in range(nbr_rows):\n",
    "        axes += [fig.add_subplot(grd[i, 0]),\n",
    "                 fig.add_subplot(grd[i, 1]),\n",
    "                 fig.add_subplot(grd[i, 2])]\n",
    "\n",
    "    colors = sb.color_palette('rocket_r', 4)\n",
    "\n",
    "    x_limits = [[1, 10], [.001, 1000], [.001, 1000]]\n",
    "    legend = [False, False, 'brief']\n",
    "\n",
    "    # Plotting data\n",
    "    for i, name in enumerate([\"alpha\", \"gamma\", \"rho\"]):\n",
    "        for j, genotype in enumerate(genotypes):\n",
    "\n",
    "            sb.lineplot(x=name, y=genotype+' Fitting Score', hue='eta',\n",
    "                        data=sim_fit, ci=None,\n",
    "                        ax=axes[3*j+i], palette=colors, legend=legend[i])\n",
    "            \n",
    "            if show_best_fit_position:\n",
    "                sb.scatterplot(x=name, y=-.0001, hue='eta',\n",
    "                               size=genotype+' Fitting Score', \n",
    "                               style=genotype+' Fitting Score',\n",
    "                               data=sim_res[best_fit[genotype] & t0],\n",
    "                               ax=axes[3*j+i], \n",
    "                               palette='rocket', \n",
    "                               sizes=[700], \n",
    "                               markers=['*'], \n",
    "                               legend=False)\n",
    "    \n",
    "        # Cosmetic\n",
    "        for j in range(nbr_rows):\n",
    "            axes[3*j+i].set_xscale('log')\n",
    "            axes[3*j+i].set_xlim(x_limits[i])\n",
    "            sb.despine(trim=True, offset=10, ax=axes[3*j+i])\n",
    "\n",
    "    # Cosmetic\n",
    "    for i in range(0, 2*nbr_rows+1, 3):\n",
    "        axes[i].xaxis.set_minor_formatter(plt.NullFormatter())\n",
    "        axes[i+2].legend(loc='center right', bbox_to_anchor=(1.5, 0.5), title='eta', ncol=1)\n",
    "\n",
    "    plt.tight_layout()\n",
    "\n",
    "    # Recording\n",
    "    if save:\n",
    "        fig_name = 'fitting_scores_allGenotypes_highResExplo.pdf'\n",
    "        fig.savefig(fig_dir+fig_name, dpi=300)\n",
    "\n",
    "        print(f'Saved graph at location: {fig_dir}, under name: {fig_name}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": true,
    "hidden": true
   },
   "source": [
    "### Comparison between experimental measurements and best-fitting simulations (Fig.1b)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We visualize on the same graph distribution of experimental measurements of seed radii for Col-0 and best-fitting simuation results.\n",
    "\n",
    "**Note:** For Fig.1b only the first plot (Col-0) has been used."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "for g in ['Col-0', 'iku2', 'ede1-3']:\n",
    "    save = True if g == 'Col-0' else False\n",
    "    plot_relative_radius(g, save=save) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Plotting Fitting scores for the parameter space exporation at constant pressure (Fig.1f & Supp. Fig.4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Note:** For the Fig.1f panel just the `'Col-0'` data have been shown and the whole three rows are displayed in Supp. Fig.4."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if pressure_function == 'Cst':\n",
    "    plot_fitting_score(['Col-0', 'ede1-3', 'iku2'], show_best_fit_position=False, save=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Plotting fitting score for parameter space exploration with time-dependent pressure (Supp. Fig. 5g)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if pressure_function == 'Var':\n",
    "    plot_fitting_score('iku2', show_best_fit_position=False, save=True)"
   ]
  }
 ],
 "metadata": {
  "kernel_info": {
   "name": "python3"
  },
  "kernelspec": {
   "display_name": "Python 3.8.13 ('seed')",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.13"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  },
  "nteract": {
   "version": "0.21.0"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": false,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {
    "height": "calc(100% - 180px)",
    "left": "10px",
    "top": "150px",
    "width": "165px"
   },
   "toc_section_display": true,
   "toc_window_display": false
  },
  "vscode": {
   "interpreter": {
    "hash": "a03340c5f2b18573e3baecfb58e1a3ba2c99cc2b4589f9ac3464355dfbcc1598"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
