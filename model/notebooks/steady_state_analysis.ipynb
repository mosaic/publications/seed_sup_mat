{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Steady state analysis\n",
    "\n",
    "### Abstract\n",
    "In this notebook we are investigating the properties of the steady state(s) admissible by the following system of *ODEs*:\n",
    "$$\n",
    "\\newcommand{\\tmin}{t_{\\text{min}}}\n",
    "\\newcommand{\\tmax}{t_{\\text{max}}}\n",
    "\\newcommand{\\dt}{\\delta t}\n",
    "\\newcommand{\\rad}{R}\n",
    "\\newcommand{\\thck}{H}\n",
    "\\newcommand{\\press}{P}\n",
    "\\newcommand{\\stress}{\\sigma}\n",
    "\\newcommand{\\strain}{\\varepsilon}\n",
    "\\newcommand{\\stiff}{K}\n",
    "\\newcommand{\\bslblk}{K_0}\n",
    "\\newcommand{\\gth}{\\varepsilon_{\\text{th}}}\n",
    "\\newcommand{\\gtm}{\\tau_{\\text{g}}}\n",
    "\\newcommand{\\sth}{\\sigma_{\\text{th}}}\n",
    "\\newcommand{\\stm}{\\tau_{\\text{s}}}\n",
    "\\newcommand{\\bsr}{k^0_{\\text{on}}}\n",
    "\\newcommand{\\dsr}{\\Delta k^{\\stress}_{\\text{on}}}\n",
    "\\newcommand{\\adt}{\\tilde{t}}\n",
    "\\newcommand{\\adrad}{r}\n",
    "\\newcommand{\\adradinf}{\\adrad_{\\infty}}\n",
    "\\newcommand{\\adstf}{k}\n",
    "\\newcommand{\\adstfinf}{\\adstf_{\\infty}}\n",
    "\\newcommand{\\adpress}{p}\n",
    "\\newcommand{\\rtm}{\\gamma}\n",
    "\\newcommand{\\rth}{\\rho}\n",
    "\\newcommand{\\fbs}{\\alpha}\n",
    "\\newcommand{\\hxp}{\\eta}\n",
    "\\newcommand{\\avgfbs}{\\bar{\\fbs}}\n",
    "\\newcommand{\\avgrtm}{\\bar{\\rtm}}\n",
    "\\newcommand{\\avgrth}{\\bar{\\rth}}\n",
    "\\newcommand{\\avghxp}{\\bar{\\hxp}}\n",
    "\\newcommand{\\micron}{\\si{\\micro \\meter}}\n",
    "\\newcommand{\\MPa}{\\si{\\mega \\pascal}}\n",
    "\\newcommand{\\second}{\\si{\\s}}\n",
    "\\newcommand{\\invsec}{\\si{\\per \\s}}\n",
    "\\newcommand{\\hill}[2]{h_{\\hxp}\\left(#1,  #2 \\right)}\n",
    "\\newcommand{\\ramp}[2]{\\big(\\frac{#1}{#2} - 1\\big)_{+}}\n",
    "$$\n",
    "\n",
    "\\begin{cases}\n",
    "    \\dot{\\adrad} = \\ramp{\\adpress\\adrad}{\\adstf} \\adrad \\\\\n",
    "    \\dot{\\adstf} = \\rtm (1 - \\adstf + \\fbs\\hill{\\adrad\\adpress}{\\rth})\n",
    "\\end{cases}\n",
    "\\label{eq:admin_sys}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Rationale:**\n",
    "Steady state configurations correspond to tuples $\\big(\\adradinf, \\adstfinf\\big)$ such that time derivatives in eq.(\\ref{eq:adim_system}) vanish.\n",
    "\n",
    "One trivial solution reads $\\big(\\adradinf, \\adstfinf\\big)= \\big(0, 1\\big)$, but more interesting ones verify the following conditions:\n",
    "\n",
    "\\begin{equation*}\n",
    "\\begin{array}{c}\n",
    "\\begin{cases}\n",
    "    \\adstfinf \\geq  \\adradinf \\adpress \\\\\n",
    "    \\adstfinf = 1 + \\alpha \\hill{\\adradinf\\adpress}{\\rth}\n",
    "\\end{cases}\\\\\n",
    "\\Updownarrow\n",
    "\\end{array}\n",
    "\\end{equation*}\n",
    "\n",
    "$$\n",
    " 1 + \\alpha \\hill{\\adradinf\\adpress}{\\rth} \\geq  \\adradinf \\adpress\n",
    "\\label{eq:steady_state}\n",
    "$$\n",
    "\n",
    "Equation ($\\ref{eq:steady_state}$) above defines a region within the configuraiton space. Its existence is conditionned by the number of its *roots*, *i.e.* specific values ($\\adradinf^{(i)}$) of $\\adradinf$ that verify:\n",
    "\n",
    "$$\n",
    "\\adradinf^{(i)} \\adpress = 1 + \\alpha \\hill{\\adradinf^{(i)}\\adpress}{\\rth}\n",
    "\\label{eq:root_condition} \n",
    "$$\n",
    "\n",
    "We are going to look for these roots (number and values) and how they depend on the values of the parameter set $\\{\\alpha, \\rho, \\eta\\}$ as well as on the pressure $\\adpress$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Setup\n",
    "\n",
    "In this section:\n",
    "* We call all the needed libraries.\n",
    "* We define the folder where all the generated graphs will be recorded.\n",
    "* We set the default parameter values for our equations."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": true
   },
   "source": [
    "### Dependencies"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "import os\n",
    "from copy import deepcopy\n",
    "\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import seaborn as sb\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "import matplotlib.gridspec as gs\n",
    "import ipywidgets as widgets\n",
    "\n",
    "from matplotlib import rc\n",
    "\n",
    "from utils import hill_function, piecewise_hill_function"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": true
   },
   "source": [
    "### I/O Directories"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "curr_work_dir = os.getcwd()\n",
    "\n",
    "# -- Where the results (DataFrames in hdf5 format) will be recorded\n",
    "res_dir = os.getcwd()[:-15]+'data/analysis_results/'\n",
    "\n",
    "# -- Where the figures (in pdf format) will be recorded\n",
    "fig_dir = curr_work_dir[:-15]+'doc/figures/'\n",
    "\n",
    "# -- Checking if output directories exist and create them if not\n",
    "for dir_path in [res_dir, fig_dir]:\n",
    "    if not os.path.isdir(dir_path):\n",
    "        os.makedirs(dir_path)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": true
   },
   "source": [
    "### Default parameter values"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "As default parameter values, we are going to use the ones corresponding to the *Col-0* best-fitting simulation obtained within the `parameter_space_exploration.ipynb` notebook."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "Let's recover the `DataFrame` of processed data generated within the `parameter_space_exploration.ipynb` notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "data = \"sim_res_cstePressure.hdf5\"\n",
    "path = res_dir + data\n",
    "\n",
    "sim_res = pd.read_hdf(path)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "Keep only the final time step of the  *Col-0* best-fitting simulation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "best = sim_res['Col-0 Fitting Rank'] == sim_res['Col-0 Fitting Rank'].min()\n",
    "final = sim_res['Time (DAP)'] == sim_res['Time (DAP)'].max()\n",
    "\n",
    "best_steady_state = sim_res[best & final]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "Extract the `DEFAULT_PARAMS` values from this best-fitting simulation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "print(f'Parameter values for Col-0 best fitting simulation:')\n",
    "    \n",
    "score = best_steady_state['Col-0 Fitting Score (Raw)'].unique()[0]\n",
    "sim_idx = best_steady_state['Replicate'].unique()[0]\n",
    "    \n",
    "print(f'    * Score (absolute value): {score:.2f}')\n",
    "print(f'    * Sim. #: {sim_idx}')\n",
    "print('    * Parameter values:')\n",
    "\n",
    "DEFAULT_PARAMS = {name: best_steady_state[name].unique()[0]\n",
    "                  for name in ['alpha', 'eta', 'gamma', 'rho', 'Pressure']}\n",
    "\n",
    "for name, val in DEFAULT_PARAMS.items():\n",
    "    print(f'        - {name}: {val:.2f}')\n",
    "\n",
    "print('\\n')\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "Let's also define a range of radii to investigate latter on"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "radii = np.arange(0, 10, .01)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": true
   },
   "source": [
    "## Analysis\n",
    "\n",
    "In this section:\n",
    "* We define functions corresponding the the *right-* and *left-hand sides* of eq.($\\ref{eq:root_condition}$).\n",
    "* We formalize the stationnary condition of eq.($\\ref{eq:root_condition}$) as a dedicated equation.\n",
    "* We compute the roots of this equation on a subdomain of the parameter space."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": true,
    "hidden": true
   },
   "source": [
    "### Equations definition"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "Below we define the *rhs* of eq.($\\ref{eq:root_condition}$):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "code_folding": [
     0
    ],
    "hidden": true
   },
   "outputs": [],
   "source": [
    "def steadystate_stiffness(radius, **kwargs):\n",
    "    \"\"\"Computes the steady state of the stiffening ODE.\n",
    "\n",
    "    Parameters\n",
    "    ----------\n",
    "    radius : float\n",
    "        The relative radius of the considered growing sphere.\n",
    "\n",
    "    Other parameters\n",
    "    ----------------\n",
    "    approx : bool\n",
    "        Default: False. If true the Hill function is replaced\n",
    "        by its piecewise approximation.\n",
    "    pressure : float\n",
    "        Default: DEFAULT_PARAMS['pressure'].\n",
    "        Relative inner pressure.\n",
    "    alpha : float\n",
    "        Default: DEFAULT_PARAMS['alpha'].\n",
    "        Mechano-sensitivity of the stiffening pathway.\n",
    "    rho : float\n",
    "        Default : DEFAULT_PARAMS['rho'].\n",
    "        Ratio between stiffening and growth thresholds.\n",
    "    eta : int\n",
    "        Default : DEFAULT_PARAMS['eta'].\n",
    "        Hill function exponent.\n",
    "\n",
    "    Returns\n",
    "    -------\n",
    "    float\n",
    "        The stiffness value at steady state.\n",
    "    \"\"\"\n",
    "    approx = kwargs.get('approx', False)\n",
    "    pressure = kwargs.get('Pressure', DEFAULT_PARAMS['Pressure'])\n",
    "    alpha = kwargs.get('alpha', DEFAULT_PARAMS['alpha'])\n",
    "    rho = kwargs.get('rho', DEFAULT_PARAMS['rho'])\n",
    "    eta = kwargs.get('eta', DEFAULT_PARAMS['eta'])\n",
    "\n",
    "    if not approx:\n",
    "        return 1 + alpha * hill_function(radius * pressure / rho, eta)\n",
    "    else:\n",
    "        return 1 + alpha * piecewise_hill_function(radius * pressure / rho, eta)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "Now, we define the *lhs* of eq.($\\ref{eq:root_condition}$)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "code_folding": [
     0
    ],
    "hidden": true
   },
   "outputs": [],
   "source": [
    "def steadystate_growth(radius, **kwargs):\n",
    "    \"\"\"Computes the steady state of the growth ODE.\n",
    "\n",
    "    Parameters\n",
    "    ----------\n",
    "    radius : float\n",
    "        The relative radius of the considered growing sphere.\n",
    "\n",
    "    Other parameters\n",
    "    ----------------\n",
    "    pressure : float\n",
    "        Optional (default : DEFAULT_PARAMS['Pressure']).\n",
    "        Relative inner pressure.\n",
    "\n",
    "    Returns\n",
    "    -------\n",
    "    float\n",
    "        The stiffness value at steady state.\n",
    "    \"\"\"\n",
    "\n",
    "    pressure = kwargs.get('Pressure', DEFAULT_PARAMS['Pressure'])\n",
    "\n",
    "    return radius * pressure"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "Finally we compare them together to define the existence condition of the stationnary state of interest."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "code_folding": [
     0
    ],
    "hidden": true
   },
   "outputs": [],
   "source": [
    "def stationnary_condition(radius, **kwargs):\n",
    "    \"\"\"Vanishes for stationnary point.\n",
    "\n",
    "    Parameters\n",
    "    ----------\n",
    "    radius : float\n",
    "        The relative radius of the considered growing sphere.\n",
    "\n",
    "    Other parameters\n",
    "    ----------------\n",
    "    approx : bool\n",
    "        Default : False. If true the Hill function\n",
    "        is replaced by its piecewise approximation.\n",
    "    pressure : float\n",
    "        Default : DEFAULT_PARAMS['Pressure'].\n",
    "        Relative inner pressure.\n",
    "    alpha : float\n",
    "        Default : DEFAULT_PARAMS['alpha'].\n",
    "        Mechano-sensitivity of the stiffening pathway.\n",
    "    rho : float\n",
    "        Default : DEFAULT_PARAMS['rho'].\n",
    "        Ratio between stiffening and growth thresholds.\n",
    "    eta : int\n",
    "        Default : DEFAULT_PARAMS['eta'].\n",
    "        Hill function exponent.\n",
    "    \n",
    "    Returns\n",
    "    -------\n",
    "    float\n",
    "        The difference between the stiffness values\n",
    "        computed with the stiffening & growth equations.\n",
    "    \"\"\"\n",
    "    return steadystate_stiffness(radius, **kwargs) - steadystate_growth(radius, **kwargs)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": true,
    "hidden": true
   },
   "source": [
    "### Computing the roots of eq.($\\ref{eq:root_condition}$)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "Let's first define some methods to find the seeked roots. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "code_folding": [
     0
    ],
    "hidden": true
   },
   "outputs": [],
   "source": [
    "def find_roots(function, interval, precision=.001, **kwargs):\n",
    "    \"\"\"Estimate the roots of a function within an interval.\n",
    "\n",
    "    Parameters\n",
    "    ----------\n",
    "    function : Python function\n",
    "        The function we want to find the roots of.\n",
    "    interval : list(float)\n",
    "        The interval ([min, max]) to examine.\n",
    "    precision : float\n",
    "        Optional (default : .01). The discretization increment on the interval.\n",
    "        \n",
    "    Other parameters\n",
    "    ----------------\n",
    "    pressure : float\n",
    "        Optional (default : DEFAULT_PARAMS['Pressure']).\n",
    "        Relative inner pressure.\n",
    "    alpha : float\n",
    "        Optional (default : DEFAULT_PARAMS['alpha']).\n",
    "        Mechano-sensitivity of the stiffening pathway.\n",
    "    rho : float\n",
    "        Optional (default : DEFAULT_PARAMS['rho']).\n",
    "        Ratio between stiffening and growth thresholds.\n",
    "    eta : int\n",
    "        Optional (default : DEFAULT_PARAMS['eta']).\n",
    "        Hill function exponent.\n",
    "\n",
    "    Returns\n",
    "    -------\n",
    "    list(float)\n",
    "        The roots.\n",
    "    \"\"\"\n",
    "\n",
    "    intrvl = np.arange(interval[0], interval[-1], precision)\n",
    "    values = list(map(lambda x: function(x, **kwargs), intrvl))\n",
    "\n",
    "    roots = []\n",
    "    it_zero = 0\n",
    "    for it, (s0, s1) in enumerate(zip(values[1:], values[:-1])):\n",
    "        if s0 * s1 < 0:\n",
    "            root = (intrvl[it] + intrvl[it+1])/2\n",
    "            roots.append(root)\n",
    "\n",
    "        elif s0 * s1 == 0:\n",
    "            if it_zero == 0:\n",
    "                it_zero = it\n",
    "                root = (intrvl[it] + intrvl[it+1])/2\n",
    "\n",
    "            elif it == it_zero + 1:\n",
    "                it_zero = 0\n",
    "                root += (intrvl[it] + intrvl[it+1])/2\n",
    "                root /=2\n",
    "                roots.append(root)\n",
    "\n",
    "    return roots"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "code_folding": [
     0
    ],
    "hidden": true
   },
   "outputs": [],
   "source": [
    "def compute_roots(parameter_name, parameter_range, precision=.01, **kwargs):\n",
    "    \"\"\"Gets the roots of the steady state condition given sets of parameters.\n",
    "    \n",
    "    Parameters\n",
    "    ----------\n",
    "    parameter_name : str\n",
    "        Name of the parameter to investigate.\n",
    "        Should be in the list: ['alpha', 'gamma', 'rho', 'eta'].\n",
    "    parameter_range : list(float)\n",
    "        The value interval ([min, max]) to proble.\n",
    "    \n",
    "    Other parameters\n",
    "    ----------------\n",
    "    approx : bool\n",
    "        Default : False. If true the Hill function\n",
    "        is replaced by its piecewise approximation.\n",
    "    algo_precision : float\n",
    "        (default : .001), the size of the searching step,\n",
    "        see documentation of the `find_roots` method.\n",
    "    radius_range : list(float)\n",
    "        (default : [0, 15]), the interval of radius values\n",
    "        to consider.\n",
    "    pressure : float\n",
    "        (default : DEFAULT_PARAMS['Pressure']),\n",
    "        relative inner pressure.\n",
    "    alpha : float\n",
    "        (default : DEFAULT_PARAMS['alpha']),\n",
    "        mechano-sensitivity of the stiffening pathway.\n",
    "    rho : float\n",
    "        (default : DEFAULT_PARAMS['rho']),\n",
    "        ratio between stiffening and growth thresholds.\n",
    "    eta : int\n",
    "        (default : DEFAULT_PARAMS['eta']),\n",
    "        hill function exponent.\n",
    "    \n",
    "    Returns\n",
    "    -------\n",
    "    pandas.DataFrame\n",
    "        Contains all the needed information, ready to plot.\n",
    "    \"\"\"\n",
    "    xmin, xmax = parameter_range\n",
    "    dx = precision\n",
    "    \n",
    "    algo_prcs = kwargs.get('algo_precision', .001)\n",
    "    radii = kwargs.get('radius_range', [0, 15])\n",
    "    \n",
    "    updated_kwargs = deepcopy(kwargs)\n",
    "    \n",
    "    nbr_previous_roots = 1\n",
    "    \n",
    "    data = []\n",
    "    bifurcation_points = []\n",
    "    for x in np.arange(xmin, xmax, dx):\n",
    "        updated_kwargs.update({parameter_name: x})\n",
    "        \n",
    "        for approx in [False, True]:\n",
    "            roots = find_roots(stationnary_condition, radii, algo_prcs, approx=approx,\n",
    "                               **updated_kwargs)\n",
    "\n",
    "            data.append(pd.DataFrame({parameter_name: x,\n",
    "                                      'root': roots,\n",
    "                                      'root index': np.arange(len(roots)),\n",
    "                                      'approximated': approx}))\n",
    "        \n",
    "            if not approx and len(roots) != nbr_previous_roots:\n",
    "                bifurcation_points.append(x)\n",
    "                nbr_previous_roots = len(roots)\n",
    "        \n",
    "    return pd.concat(data, ignore_index=True), bifurcation_points"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "Now, we are going to swipe each parameter ($\\alpha, \\rho, \\eta$) over a wide range of values and compute the corresponding roots. The idea is to observe, the appearance/deappearance of some of these roots as function of these parameters, such process could be refered to as a *bifurcation*."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "df_alpha, bifurcation_alpha = compute_roots('alpha', parameter_range=[1, 11])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "df_rho, bifurcation_rho = compute_roots('rho', parameter_range=[1, 10])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "df_eta, bifurcation_eta = compute_roots('eta', parameter_range=[1, 9])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "We are also going to investigate the influence of the controle parameeter $\\adpress$ over the roots."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "df_pressure, bifurcation_pressure = compute_roots('Pressure', parameter_range=[.8, 2], precision=.005)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": true
   },
   "source": [
    "## Visualization\n",
    "\n",
    "In this section:\n",
    "* We produce various figures to visualize the steady state region (within the configuration space) and qualitatively show how its existence depends on the parameter values.\n",
    "* We also visualize the dependency of the roots of eq.($\\ref{eq:root_condition}$) on the parameter values."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Definition of some visualization functions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "We define a function to visualize the *state space* and how the steady state region is evolving as a function of the various parameters — & external variable — of the problem."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "code_folding": [
     0
    ],
    "hidden": true
   },
   "outputs": [],
   "source": [
    "def plot_state_space(axe=None, **kwargs):\n",
    "    \"\"\"Generates plots of the steady state conditions within the configuration space.\n",
    "    \n",
    "    Parameters\n",
    "    ----------\n",
    "    axe : matplotlib.axe\n",
    "        Optional (default : None). \n",
    "        Graph object to plot the curves on.\n",
    "    \n",
    "    Other parameters\n",
    "    ----------------\n",
    "    save : bool\n",
    "        Default : False. If True, a pdf caption of the graph is recorded.\n",
    "    display_labels : bool\n",
    "        Defaut : True. If True, name of the intersection points between\n",
    "        the two curves of interest are displayed.\n",
    "    display_steady_state : bool\n",
    "        Default : True. If True, the steady state zone, where the system \n",
    "        converges, is highlighted.\n",
    "    linewidth : int\n",
    "        Default : 2. Width of the curves and lines.\n",
    "    \n",
    "    \"\"\"\n",
    "    save = kwargs.get('save', False)\n",
    "    display_labels = kwargs.get('display_labels', True)\n",
    "    display_steady_state = kwargs.get('display_steady_state', True)\n",
    "    lw = kwargs.get('linewidth', 2)\n",
    "    \n",
    "    root_radii = find_roots(stationnary_condition, radii, **kwargs)\n",
    "    root_stiffnesses = [steadystate_stiffness(r, **kwargs) for r in root_radii]\n",
    "    \n",
    "    growth = pd.DataFrame({'radius': radii,\n",
    "                           'growth': [steadystate_growth(r, **kwargs)\n",
    "                                      for r in radii]})\n",
    "\n",
    "    stiffness = pd.DataFrame({'radius': radii,\n",
    "                              'stiffness': [steadystate_stiffness(r, **kwargs)\n",
    "                                            for r in radii]})\n",
    "\n",
    "    # --\n",
    "    colors = sb.color_palette('rocket', 10)\n",
    "    clrs = [colors[7], colors[5], colors[3]]\n",
    "    lbl = 'Stiffnening arrest condition'\n",
    "    \n",
    "    if kwargs.get('approx', False):\n",
    "        ls = ':'\n",
    "        lbl += '\\n(pw linear approx.)'\n",
    "    else:\n",
    "        ls = '-'\n",
    "    \n",
    "    if axe is None:\n",
    "        sb.set(context='talk', style='white')\n",
    "        fig = plt.figure(figsize=(7.5, 5))\n",
    "        axe = fig.add_subplot(111)\n",
    "\n",
    "    if len(axe.lines) == 0:\n",
    "        axe = sb.lineplot(x='radius', y='growth', data=growth, lw=0)\n",
    "\n",
    "        x1 = axe.lines[0].get_xydata()[:, 0]\n",
    "        y1 = axe.lines[0].get_xydata()[:, 1]\n",
    "\n",
    "        axe.fill_between(x1, y1, 12.5, color=colors[0], alpha=.10, label='Growth arrest condition')\n",
    "    \n",
    "    if 'pressure' in kwargs.keys():\n",
    "        axe = sb.lineplot(x='radius', y='growth', data=growth,\n",
    "                          color=colors[0], lw=lw, ls=':')\n",
    "        x1 = axe.lines[-1].get_xydata()[:, 0]\n",
    "        y1 = axe.lines[-1].get_xydata()[:, 1]\n",
    "        axe.fill_between(x1, y1, 12.5, color=colors[0], alpha=.05, label='Growth arrest condition')\n",
    "    \n",
    "    # --\n",
    "    if len(root_radii) > 1: \n",
    "        steady_interval = np.arange(root_radii[1], root_radii[-1], .01)\n",
    "    \n",
    "        steady_state = pd.DataFrame({'radius': steady_interval,\n",
    "                                     'stiffness': [steadystate_stiffness(r, **kwargs)\n",
    "                                                   for r in steady_interval]})\n",
    "        if display_steady_state:\n",
    "            plt.text(3, 8, r'$\\mathcal{S}_s$')\n",
    "            axe = sb.lineplot(x='radius', y='stiffness', data=steady_state,\n",
    "                              color=colors[-1], lw=5*lw, label='Steady state region')\n",
    "\n",
    "    axe = sb.lineplot(x='radius', y='stiffness', data=stiffness,\n",
    "                      color=colors[0], lw=lw, ls=ls, label=lbl)\n",
    "\n",
    "\n",
    "\n",
    "    for i, (r, k, c) in enumerate(zip(root_radii, root_stiffnesses, clrs)):\n",
    "        plt.plot(r, k, marker='o', lw=0, color=c)\n",
    "        \n",
    "        if display_labels:\n",
    "            plt.text(r, k-1, f'$I_{i}$', fontsize=16, color=c)\n",
    "\n",
    "\n",
    "    # Cosmetic\n",
    "    plt.xlabel('$r$')\n",
    "    plt.ylabel('$k$')\n",
    "    plt.ylim([0, 12])\n",
    "\n",
    "    sb.despine(trim=True)\n",
    "\n",
    "    # Recording\n",
    "    if save:\n",
    "        fig_name = 'steadyState'\n",
    "        \n",
    "        if display_labels:\n",
    "            fig_name += '_withLabels'\n",
    "            \n",
    "        if kwargs.get('approx', False):\n",
    "            fig_name += '_withPwApprox'\n",
    "            \n",
    "        if display_steady_state:\n",
    "            fig_name += '_highLighted'\n",
    "            \n",
    "        fig_name += '.pdf'\n",
    "        \n",
    "        plt.savefig(fig_dir+fig_name)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "We now wrap this function into another one to generate bundles of curves when a parameter is varying."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "code_folding": [
     0
    ],
    "hidden": true
   },
   "outputs": [],
   "source": [
    "def plot_bundle(var, values, save=False):\n",
    "    \"\"\"Generates a family of curves of various values of a parameter.\n",
    "    \n",
    "    Parameters\n",
    "    ----------\n",
    "    var: str\n",
    "        Name of the parameter to variate.\n",
    "    values: (float, float)\n",
    "        Min and max values of the range to swipe.\n",
    "    save : bool\n",
    "        Default : False. If True, a pdf caption of the graph is recorded.\n",
    "    \"\"\"\n",
    "    \n",
    "    sb.set(context='talk', style='white')\n",
    "    fig = plt.figure(figsize=(7.5, 5))\n",
    "    axe = fig.add_subplot(111)\n",
    "    \n",
    "    # --\n",
    "    if len(values) == 3:\n",
    "        vmin, vmax, dv = values\n",
    "    \n",
    "    elif len(values) == 2:\n",
    "        vmin, vmax = values\n",
    "        dv = 1\n",
    "    \n",
    "    arw = {'Pressure': [7, 6, -6, 0],\n",
    "           'alpha': [9, 1, 0, 10],\n",
    "           'rho': [.5, 5.5, 8, 0],\n",
    "           'eta': [6.5, 6.5, -1, 4]}\n",
    "    \n",
    "    txt = {'Pressure': [6, 5, r'$p$'],\n",
    "           'alpha': [9.2, 10.5, r'$\\alpha$'],\n",
    "           'rho': [8, 4.7, r'$\\rho$'],\n",
    "           'eta': [5.8, 10.7, r'$\\eta$']}\n",
    "    \n",
    "    # --\n",
    "    for i, v in enumerate(np.arange(vmin, vmax, dv)):\n",
    "        plot_state_space(axe, display_labels=False, display_steady_state=False,\n",
    "                         linewidth=1 + .2*i, **{var: v})\n",
    "    \n",
    "    # --\n",
    "    x, y, dx, dy = arw[var]\n",
    "    plt.arrow(x, y, dx, dy, color='gray', linewidth=1, head_width=.3)\n",
    "    \n",
    "    x, y, t = txt[var]\n",
    "    plt.text(x, y, t, color='gray')\n",
    "    \n",
    "    axe.get_legend().remove()\n",
    "    \n",
    "    # --\n",
    "    if save:\n",
    "        fig_name = 'steadyState_bundle_'\n",
    "\n",
    "        fig_name += var\n",
    "\n",
    "        fig_name += '.pdf'\n",
    "\n",
    "        plt.savefig(fig_dir+fig_name)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": true,
    "hidden": true
   },
   "source": [
    "### System representation within the configuration space "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": true,
    "hidden": true
   },
   "source": [
    "#### Visualization of the steady state region (Supp. Fig.3a)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "sb.set(context='talk', style='white')\n",
    "fig = plt.figure(figsize=(7.5, 5))\n",
    "axe = fig.add_subplot(111)\n",
    "\n",
    "plot_state_space(axe, approx=True, display_labels=False, display_steady_state=False)\n",
    "plot_state_space(axe, save=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": true,
    "hidden": true
   },
   "source": [
    "#### Influence of $\\alpha$ (Supp. Fig.3b)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "plot_bundle('alpha', (1, 10), save=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": true,
    "hidden": true
   },
   "source": [
    "#### Influence of $\\rho$ (Supp. Fig.3c)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "plot_bundle('rho', (1, 10), save=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": true,
    "hidden": true
   },
   "source": [
    "#### Influence of $\\eta$ (Supp. Fig.3d)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "plot_bundle('eta', (1, 9), save=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": true,
    "hidden": true
   },
   "source": [
    "#### Influence of pressure (Supp. Fig.3h)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "plot_bundle('Pressure', (1.1, 2.2, .1), save=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "Let's first define a function encompassing the necessary code to plot proper graphs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "code_folding": [
     0
    ],
    "hidden": true
   },
   "outputs": [],
   "source": [
    "def plot_root_bifurcation(parameter_name, save=False, with_bifurcation_values=False):\n",
    "    \"\"\"Displays the roots bifurcation as the parameter varies. \n",
    "    \"\"\"\n",
    "    sb.set(context='talk', style='white')\n",
    "    plt.figure(figsize=(7.5, 5))\n",
    "    colors = sb.color_palette('rocket', 10)\n",
    "    clrs = [colors[7], colors[5], colors[3]]\n",
    "\n",
    "    if parameter_name == 'alpha':\n",
    "        data = df_alpha\n",
    "        bifurcation_abs = bifurcation_alpha\n",
    "        xlabel = r'$\\alpha$'\n",
    "        txt_pos = [(5., 1.), (5., 2.8), (5., 5.2)]\n",
    "    elif parameter_name == 'rho':\n",
    "        data = df_rho\n",
    "        bifurcation_abs = bifurcation_rho\n",
    "        xlabel = r'$\\rho$'\n",
    "        txt_pos = [(5., 1.), (5., 3.4), (5., 8.7)]\n",
    "    elif parameter_name == 'eta':\n",
    "        data = df_eta\n",
    "        bifurcation_abs = bifurcation_eta\n",
    "        xlabel = r'$\\eta$'\n",
    "        txt_pos = [(5., 1.), (5., 3.4), (5., 8.7)]\n",
    "    elif parameter_name == 'Pressure':\n",
    "        data = df_pressure\n",
    "        bifurcation_abs = bifurcation_pressure\n",
    "        xlabel = r'$p$'\n",
    "        txt_pos = [(.8, 1.8), (.8, 3.4), (.8, 11.8)]\n",
    "    \n",
    "    approx = data['approximated']\n",
    "    non_approx = data['approximated'] == False\n",
    "    \n",
    "    # --\n",
    "    \n",
    "    ax = sb.lineplot(x=parameter_name, y='root', hue='root index',\n",
    "                     data=data[non_approx],\n",
    "                     linestyle='', marker='.', markersize=5,\n",
    "                     mec='none', palette=clrs, legend=False)\n",
    "    \n",
    "    ax = sb.lineplot(x=parameter_name, y='root', hue='root index',\n",
    "                     data=data[approx],\n",
    "                     linestyle='', marker='.', markersize=5,\n",
    "                     mec='none', palette=clrs, alpha=.05, legend=False)\n",
    "    \n",
    "    ax = sb.scatterplot(x=parameter_name, y='Relative Radius',\n",
    "                        data=best_steady_state, color=colors[0], marker='*', s=500)\n",
    "    \n",
    "    x1 = ax.lines[1].get_xydata()[:, 0]\n",
    "    y1 = ax.lines[1].get_xydata()[:, 1]\n",
    "    y2 = ax.lines[2].get_xydata()[:, 1]\n",
    "\n",
    "    ax.fill_between(x1, y1, y2,\n",
    "                    color=colors[0], alpha=.1)\n",
    "    \n",
    "    if with_bifurcation_values:\n",
    "        for x in bifurcation_abs:\n",
    "            y0 = ax.lines[0].get_xydata()[-1, 1]\n",
    "\n",
    "            plt.plot([x, x], [y0, 8], color='gray', linestyle=':')\n",
    "            plt.text(x+.1, 4, xlabel+f' = {x:.2f}')\n",
    "    \n",
    "    # --\n",
    "    \n",
    "    for i, (xpos, ypos) in enumerate(txt_pos):\n",
    "        plt.text(xpos, ypos, f'$I_{i}$', fontsize=16, color=clrs[i])\n",
    "    \n",
    "    plt.xlabel(xlabel)\n",
    "    plt.ylabel(r'$r_{\\infty}$')\n",
    "    sb.despine(trim=True)\n",
    "    plt.tight_layout()\n",
    "    \n",
    "    # --\n",
    "    if save:\n",
    "        fig_name = f'stop_cond_roots_{parameter_name}'\n",
    "        if with_bifurcation_values:\n",
    "            fig_name += '_with_bifurcation_values'\n",
    "        fig_name += '.pdf'\n",
    "        plt.savefig(fig_dir+fig_name)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": true,
    "hidden": true
   },
   "source": [
    "### Root bifurcation visualization"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": true,
    "hidden": true
   },
   "source": [
    "#### Influence of pressure on root existence (Supp. Fig.3i)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "plot_root_bifurcation('Pressure', save=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": true,
    "hidden": true
   },
   "source": [
    "#### Influence of $\\rho$ on root existence (Supp. Fig.3e)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "plot_root_bifurcation('rho', with_bifurcation_values=True, save=True)#"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": true,
    "hidden": true
   },
   "source": [
    "#### Influence of $\\alpha$ on root existence (Supp. Fig.3f)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "plot_root_bifurcation('alpha', with_bifurcation_values=True, save=True)#"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": true,
    "hidden": true
   },
   "source": [
    "#### Influence of $\\eta$ on root existence (Supp. Fig.3g)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "plot_root_bifurcation('eta', with_bifurcation_values=True, save=True)#"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3.8.13 ('seed')",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.13"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": false,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  },
  "vscode": {
   "interpreter": {
    "hash": "a03340c5f2b18573e3baecfb58e1a3ba2c99cc2b4589f9ac3464355dfbcc1598"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
