# -- General imports
import numpy as np

############################################
# -- Default values for main parameters -- #
############################################

DEFAULT_PARAMS = {'pressure': 1, 'alpha': 6.3, 'gamma': 9.7,
                  'rho': 5.1, 'eta': 9}

DEFAULT_TIME_STEPS = time_steps = np.arange(0, 10, .01)

##########################
# -- Useful functions -- #
##########################


def closest(value, array=DEFAULT_TIME_STEPS):
    """Finds an element from an array closest to a given value.
    """
    idx = np.searchsorted(array, value, side="left")
    if idx > 0 and (idx == len(array) or np.abs(value - array[idx-1])
                    < np.abs(value - array[idx])):
        return array[idx-1]
    else:
        return array[idx]


def hill_function(var, exp):
    """Computes the Hill function.
    """

    x = var ** exp

    return x / (1 + x)


def piecewise_hill_function(var, exp):
    """Computes a piecewise approximation of the Hill function.
    """
    if var < 1 - 2/exp:
        return 0
    elif var > 1 + 2/exp:
        return 1
    else:
        return (exp * (var - 1) + 2) / 4
   

def step(variable):
    """Implements the Heaviside step function.
    """
    if variable > 0:
        return 1
    else:
        return 0


def ramp(variable):
    """Implements the ramp function.
    """
    if variable > 0:
        return variable
    else:
        return 0


def line(variable, pts0, pts1):
    """Defines the linear function between pts0 and pts1.
    """
    x0, y0 = pts0
    x1, y1 = pts1

    a = (y1 - y0) / (x1 - x0)
    return a * (variable- x0) + y0


def hill_derivative(radius, **kwargs):
    """Implements the derivative of the Hill function in our problem.

    Parameters
    ----------
    radius : float
        The current value of the relative radius.

    Other parameters
    ----------------
    pressure : float
        Optional (default : 1). Osmotic pressure relative value.
    alpha : float
        Optional (default : 8). Mechano-sensitivity of the stiffening pathway.
    rho : float
        Optional (default : 5). Ratio between stiffening and growth thresholds.
    eta : int
        Optional (default : 6). Hill function exponent.

    Returns
    -------
    float
        The value of the derivative at the considered position.
    """
    pressure = kwargs.get('pressure', DEFAULT_PARAMS['pressure'])
    eta = kwargs.get('eta', DEFAULT_PARAMS['eta'])
    rho = kwargs.get('rho', DEFAULT_PARAMS['rho'])

    x = (radius * pressure / rho) ** (-(eta + 1))
    prefactor = pressure * eta / (rho)

    return prefactor * x / (1 + x)**2


def random_time_function(mean, std_dev, freq=1,
                         time_series=DEFAULT_TIME_STEPS, sign=None):
    """Returns a dict ofrandom values given a mean, a st. dev. & a frequency.

    Parameters
    ----------
    mean : float
        The mean value around which the random sampling will be done.
    std_dev : float
        The standard deviation of the random sampling.
    freq: int
        Optional (default : 1). The persistence of each random value.
        i.e. the value is changed every `freq` time steps.
    time_series: list or ndarray of floats
        Optional (default : time_steps - of the ODEs resolution scheme...).
        List of the time steps on which we want to sample  randomly.
    sign: str
        Optional (default : None). Possible values :{None, '+', '-', 'sd'}.
        If sign=='+' only the random values above the mean will be considered.
        If sign=='-', only the ones below.
        If sign=='sd'. The sign of the st. dev. tells which case to consider.

    Returns
    ----------------
    dict:
        - keys : float. the time steps values.
        - values : float. the corresponding of random values.
    """
    rndm = []

    for idx, _ in enumerate(time_series):

        if idx % freq == 0:

            new_val = np.random.normal(mean, np.abs(std_dev))

            if sign == '+' or (sign == 'sd' and std_dev >= 0):
                new_val = max(mean, new_val)
            elif sign == '-'or (sign == 'sd' and std_dev < 0):
                new_val = min(mean, new_val)

            rndm.append(new_val)

        else:
            rndm.append(rndm[-1])

    return {time: rndm_val for time, rndm_val in zip(time_series, rndm)}


def d_state(time, state, **kwargs):
    '''Implements the differential system we want to solve.

    Parameters
    ----------
    time : float
        the variable we differentiate along.
    state: list(float)
        the vector describing the state of the system.

    Other parameters
    ----------------
    pressure : float
        Optional (default : 1). Osmotic pressure relative value.
    alpha : float
        Optional (default : 8). Mechano-sensitivity of the stiffening pathway.
    gamma : float
        Optional (default : 1). Ratio between growth and stiffening time.
    rho : float
        Optional (default : 5). Ratio between stiffening and growth thresholds.
    eta : int
        Optional (default : 6). Hill function exponent.

    Returns
    -------
    d_state : list(float)
            the vector of the derivative of the state variables.
    '''
    radius, stiffness = state

    param = {}
    for name in ['pressure', 'alpha', 'gamma', 'rho', 'eta']:
        param[name] = kwargs.get(name, DEFAULT_PARAMS[name])

    d_radius = ramp(param['pressure'] * radius / stiffness - 1) * radius
    d_stiffness = param['gamma'] * (1 - stiffness + param['alpha']
                                    * hill_function(param['pressure']
                                                    * radius / param['rho'],
                                                    param['eta']))

    return [d_radius, d_stiffness]
