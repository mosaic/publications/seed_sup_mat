# --*-- coding: utf-8 --*--
# --*-- python --*--
#
# @Author: Olivier Ali  <oali>
# @Email:  olivier.ali@inria.fr

"""
This script generates simulations for various values of parameters.
"""

import os
import sys
import getopt
import warnings

import numpy as np

from datetime import datetime
from pypet import Environment, cartesian_product

from utils import DEFAULT_PARAMS, pypet_diff_sys_solver_wrapper

warnings.simplefilter(action='ignore', category=FutureWarning)

if __name__ == '__main__':
    # -- Checking the command line arguments
    core_number = 20
    test = False
    eta = 7
    pfunc = 'cste'  # 'cste', 'generic_drop', 'pwl_drop'

    for opt, arg in getopt.getopt(sys.argv[1:], "tn:e:p:")[0]:
        if opt == "-n":
            core_number = int(arg)
        if opt == "t":
            test = True
        if opt == "-e":
            eta = int(arg)
        if opt == "-p":
            pfunc = arg

    # -- Setting the recorded file name and directory
    start_time = datetime.now()
    date_stamp = start_time.strftime("%Y%m%d_%H%M%S")[2:]

    traj_name = 'seed_growth'
    folder_name = date_stamp + '_' + traj_name + '/'
    record_dir_path = os.getcwd()[:-13] + 'data/data_param_space_explo/'
    record_dir_path += folder_name

    if not os.path.isdir(record_dir_path):
        os.mkdir(record_dir_path)

    os.chdir(record_dir_path)

    # -- Setting up the simulation environment
    env = Environment(trajectory=traj_name,
                      filename='./hdf5/raw_trajectory.hdf5',
                      multiproc=True,
                      ncores=core_number,
                      use_pool=True,
                      wrap_mode='QUEUE')

    traj = env.traj

    # -- Defining the time interval of the simulations
    tmin = 0.
    tmax = 3.
    dt = .02
    nbr_time_steps = (tmax - tmin) / dt

    traj.f_add_parameter('tmin', tmin, comment='tmin')
    traj.f_add_parameter('tmax', tmax, comment='tmax')
    traj.f_add_parameter('dt', dt, comment='dt')

    # -- Defining the pressure function to consider
    traj.f_add_parameter('pfunc', pfunc, comment='considered pressure function')

    # -- Defining the initial state of the system
    init_state = np.array([1.5, 1.])
    traj.f_add_parameter('init_state', init_state, comment='[r0, k0]')

    # -- Adding default values to the various parameters
    for name, (value, description) in DEFAULT_PARAMS.items():
        traj.f_add_parameter(name, value, comment=description)

    # -- Define the parameter space to explore
    if test: # Generates a small batch to test the pipeline.
        param_space = {'alpha': [float(a) for a in np.arange(6, 7, .2)],
                       'gamma': [float(10**g) for g in np.arange(0, 1, .2)],
                       'rho': [float(10**r) for r in np.arange(0, 1, .2)],
                       'eta': [eta]}

    else: # Generates the big batch on the full region to explore.
        param_space = {'alpha': [float(a)
                                 for a in np.arange(1, 11, .2)],
                       'gamma': [float(10**g)
                                 for g in np.arange(-2.5, 2.5, .1)],
                       'rho': [float(10**r)
                               for r in np.arange(-2.5, 2.5, .1)],
                       'eta': [eta]}

    traj.f_explore(cartesian_product(param_space))

    # -- Run simulations
    env.run(pypet_diff_sys_solver_wrapper)

    # -- Execution time
    print(str(datetime.now() - start_time))
