# --*-- coding: utf-8 --*--
# --*-- python --*--
#
# @Author: Olivier Ali  <oali>
# @Email:  olivier.ali@inria.fr

"""
This script format simulation outputs as pandas dataframe.

NOTE
----
* This script supports command line arguments. It should be called as:
    `$: result_formatting.py -i <foldername>`
where `<foldername>` is the name of a folder containing a raw data file
named `raw_trajectory.hdf5`.

"""
import os
import sys
import getopt
import time

from pypet import load_trajectory

from utils import format_results

if __name__ == '__main__':
    # -- Set initial time
    t0 = time.time()

    # -- Loading the raw trajectory
    results_path = os.getcwd()[:-13] + 'data/data_param_space_explo/'

    # N.B.: The 10 lines below enables to set the folder_name variable
    #       Directly from the command line call.
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hi:")
    except getopt.GetoptError:
        print('result_formatting.py -i <inputfoldername>')
        sys.exit(2)

    for opt, arg in opts:
        if opt == "-i":
            folder_name = arg
        else:
            print('WARNING: result_formatting.py -i <inputfoldername>')
            sys.exit()

    path_to_file = results_path + folder_name + '/hdf5/raw_trajectory.hdf5'

    traj_name = folder_name[14:]

    traj = load_trajectory(filename=path_to_file, name=traj_name, load_all=2,
                           force=True)
    # N.B.: The force=True argument is here to force the compatibility
    #       between different versions of pandas.

    # -- Formatting the data
    df = format_results(traj)

    # -- Saving the formatted dataframe
    record_path = results_path + folder_name + '/hdf5/formatted_dataframe.hdf5'

    df.to_hdf(record_path, key='df', mode='w')

    # -- Formatting time estimation
    dt = time.time() - t0
    dh = dt//3600
    dm = dt % 3600//60
    ds = dt % 60

    print(f"Data processed in {dh} h, {dm} min and {ds} sec")
