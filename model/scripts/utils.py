# --*-- coding: utf-8 --*--
# --*-- python --*--
#
# @Author: Olivier Ali  <oali>
# @Email:  olivier.ali@inria.fr

"""
This file contains all the methods needed to perform
the parameter space exploration on our system of ODEs.
"""
import numpy as np
import pandas as pd

from scipy.integrate import solve_ivp

# -- Default parameter values
DEFAULT_PARAMS = {'pressure': (1.2, 'Turgor pressure'),
                  'alpha': (8., 'Feedback efficiency'),
                  'rho': (5., 'Characteristic time ratio'),
                  'gamma': (1., 'Threshold ratio'),
                  'eta': (9, 'Hill function exponent')}


# -- Useful mathematical functions
def hill_function(var, exp):
    """Computes the Hill function.
    """
    x = var ** exp

    return x / (1 + x)


def ramp(variable):
    """Implements the ramp function.
    """
    if variable > 0:
        return variable
    else:
        return 0


def line(variable, pts0, pts1):
    """Defines the linear function between pts0 and pts1.
    """
    x0, y0 = pts0
    x1, y1 = pts1

    a = (y1 - y0) / (x1 - x0)
    return a * (variable- x0) + y0


def generic_pressure_drop(time, p0=1.2, dp=.20, tp=.90, xp=5):
    '''Implement a generic time-dependent pressure drop.

    Parameters
    ----------
    time : float
        time variable.
    p0 : float
        Optional (default : 1.). The initial pressure value.
    dp : float
        Optional (default : .58). Percentage of the pressure drop between
        initial and final value.
    tp : float
        Optional (default : .68). Time at which half of the pressure
        drop has been reached.
    xp : int
        Optional (default : 2). Exponent of the hill function used
        to implement this pressure drop.

    Returns
    -------
    float
        the pressure value at the considered time.

    Note
    ----
    The default variable values corresponds to a moderate pressure drop 
    that is loosely similar to the one observed in iku2 mutants:
    p0=1.2, dp=.20, tp=.90, xp=5
    '''
    return p0 * (1 - dp * hill_function(time/tp, xp))


def pwl_pressure_drop(time, genotype, replicate='merged', mix=.5):
    """Defines a piecewise linear pressure function interpolated from measurements.

    Parameters
    ----------
    time : float
        time variable.
    genotype : str
        The data to consider. Should be in ['Col-0', 'iku2', 'mixed']
    replicate : int | str
        Optional (default : 'merged'). The replicate of the measures to consider.
        Should either be in [1,2,3,4] (choose one of these replicates) or 'merged'.
        If 'merged' is chosen, the considered values are averaged of all replicates.
    mix : float
        Optional (default : .5). Should be a value between 0 and 1. if 0 the function
        interpolate iku2 data, if 1 it interpolotes Col-0 data and inbetween it
        generates a mix of the two.

    Returns
    -------
    float
        the pressure value at the considered time.
    """

    pressure_data = {'Col-0': {1: [(1.5, 1.07), (3.5, 0.85), (5.5, 0.715), (7.0, 0.62)],
                               2: [(1.5, 0.877), (3.5, 0.813), (5.5, 0.577), (7.0, 0.963)],
                               3: [(1.5, 1.227), (3.5, 1.023), (5.5, 0.617), (7.0, 0.552)],
                               4: [(1.5, 1.2), (3.5, 1.176), (5.5, 0.671), (7.0, 0.467)],
                               'merged': [(1.5, 1.1), (3.5, 0.939), (5.5, 0.632), (7.0, 0.639)]},
                     'iku2': {1: [(1.5, 1.2), (3.5, 1.079), (5.5, 1.043), (7.0, 1.036)],
                              2: [(1.5, 1.2), (3.5, 1.08), (5.5, 0.983), (7.0, 0.767)],
                              3: [(1.5, 1.2), (3.5, 1.164), (5.5, 0.979), (7.0, 0.89)],
                              4: [(1.5, 1.448), (3.5, 1.448), (5.5, 1.061), (7.0, 1.023)],
                              'merged': [(1.5, 1.2), (3.5, 1.133), (5.5, 1.019), (7.0, 0.963)]}}

    if genotype == 'mixed':
        assert (mix>=0) & (mix<=1)
        
        time_pressure_steps = [(t, mix*v2 +(1-mix)*v1) 
                               for (t, v1), (_, v2)
                               in zip(pressure_data['iku2'][replicate], pressure_data['Col-0'][replicate])]
    else:
        time_pressure_steps = pressure_data[genotype][replicate]
    
    tmin, p0 = time_pressure_steps[0]
    if time < tmin:
        return p0

    for i, pts1 in enumerate(time_pressure_steps):
        if time <= pts1[0]:
            pts0 = time_pressure_steps[i-1]
            return line(time, pts0, pts1)
    
    return pts1[1]


# -- System definition (for parameter space exploration)
def rhs_func(time, state, **kwargs):
    '''Implements the right hand-side part of the differential system to solve.

    Parameters
    ----------
    time : float
        the variable we differentiate along.
    state: list(float)
        the vector describing the state of the system.

    Other parameters
    ----------------
    pressure : float
        Optional (default : 1). Osmotic pressure relative value.
    alpha : float
        Optional (default : 8). Mechano-sensitivity of the stiffening pathway.
    gamma : float
        Optional (default : 1). Ratio between growth and stiffening time.
    rho : float
        Optional (default : 5). Ratio between stiffening and growth thresholds.
    eta : int
        Optional (default : 6). Hill function exponent.

    Returns
    -------
    rhs_func : list(float)
            the vector of the derivative of the state variables.
    '''
    radius, stiffness = state

    param = {}
    for name in ['pressure', 'alpha', 'gamma', 'rho', 'eta']:
        param[name] = kwargs.get(name, DEFAULT_PARAMS[name])

    d_radius = ramp(param['pressure'] * radius / stiffness - 1) * radius

    d_stiffness = 1 - stiffness
    d_stiffness += param['alpha'] * hill_function(param['pressure'] * radius
                                                  / param['rho'], param['eta'])
    d_stiffness *= param['gamma']

    return [d_radius, d_stiffness]


def diff_sys_solver(initial_state, tmin, tmax, dt,
                    pressure, alpha, gamma, rho, eta,
                    pressure_function='cste'):
    """Solves an ODE system over a time interval, given an initial state.

    Parameters
    ----------
    tmin : float
        tmin to consider in the simulation.
    tmax : float
        tmax to consider in the simulation.
    dt : float
        dt to consider in the simulation.
    init_state : array(float)
        Initial value to start from.
    results : DataFrame
        The dataframe where the simulation parameters and results are stored.
    pressure : float
        Osmotic pressure relative value.
    alpha : float
        Mechano-sensitivity of the stiffening pathway.
    gamma : float
        Ratio between growth and stiffening time.
    rho : float
        Ratio between stiffening and growth thresholds.
    eta : int
        Hill function exponent.
    pressure_function : str
        Optional (default is 'Cste). States which pressure function to consider
        Should be in ['cste', 'generic_drop', 'pwl_drop']

    Returns
    -------
    results : dict
        - keys : str. The parameter & output names of the differential system.
        - values : float, int and/or nDarray(float)

    Notes
    -----
    The keys of the returned dict are: 'pressure', 'alpha', 'gamma', 'eta',
    'rho', 'time', 'radius', 'stiffness'.
    """
    
    if pressure_function == 'cste':
        def diff_sys(time, state):
            return rhs_func(time, state, pressure=pressure, alpha=alpha,
                            gamma=gamma, rho=rho, eta=eta)
    
    elif pressure_function == 'generic_drop':
        def diff_sys(time, state):
            return rhs_func(time, state,
                            pressure=generic_pressure_drop(time),
                            alpha=alpha, gamma=gamma, rho=rho, eta=eta)
    
    elif pressure_function == 'pwl_drop':
        def diff_sys(time, state):
            return rhs_func(time, state, 
                            pressure=pwl_pressure_drop(5*time, 
                                                       genotype='iku2',
                                                       replicate='merged'),
                            alpha=alpha, gamma=gamma, rho=rho, eta=eta)

    sol = solve_ivp(diff_sys, (tmin, tmax), initial_state,
                    t_eval=np.arange(tmin, tmax, dt))

    return sol.y.T


def pypet_diff_sys_solver_wrapper(traj):
    """Passes parameters to and results from pypet.

    Parameters
    ----------
    traj : pypet.Trajectory
        The pyper container of the simulations.

    Returns
    -------
    None.
    """

    sim_output = diff_sys_solver(traj.init_state, traj.tmin, traj.tmax,
                                 traj.dt, traj.pressure, traj.alpha,
                                 traj.gamma, traj.rho, traj.eta, traj.pfunc)

    traj.f_add_result('sim_output', sim_output,
                      comment='Simu outputs from the grwth/stiff model.')


# -- Formatting results
def format_results(traj):
    """Stores the parameter space exploration results into a dataframe.

    Parameters
    ----------
    traj : pypet.Trajectory
        The trajectory containing the results to sort and process.

    Returns
    -------
    pandas.DataFrame
        a dataframe containing all the parameter permutations
        and the corresponding results.

    """

    tmin = traj.par.f_get('tmin').f_get()
    tmax = traj.par.f_get('tmax').f_get()
    dt = traj.par.f_get('dt').f_get()

    df = pd.DataFrame(index=None, columns=['Pressure', 'alpha', 'gamma', 'rho',
                                           'eta', 'Time', 'Radius',
                                           'Stiffness'])

    for idx, run_name in enumerate(traj.f_iter_runs()):

        sim_output = traj.res.runs.crun.sim_output

        par_and_res = {'time': np.arange(tmin, tmax, dt),
                       'radius': sim_output[:, 0],
                       'stiffness': sim_output[:, 1]}

        for param in traj.par:
            p_name = param.v_name
            if len(param) > 1:
                par_and_res[p_name] = traj.par.f_get(p_name).f_get_range()[idx]
            else:
                par_and_res[p_name] = traj.par.f_get(p_name).f_get()

        df = df.append(par_and_res, ignore_index=True)

    return df
